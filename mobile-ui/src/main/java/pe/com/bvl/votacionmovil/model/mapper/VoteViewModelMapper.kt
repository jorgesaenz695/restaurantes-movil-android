package pe.com.bvl.votacionmovil.model.mapper

import pe.com.bvl.votacionmovil.model.VoteViewModel
import pe.com.bvl.votacionmovil.presentation.model.VoteView
import javax.inject.Inject

/**
 * Map a [VoteView] to and from a [VoteViewModel] instance when data is moving between
 * this layer and the Domain layer
 */
open class VoteViewModelMapper @Inject constructor() : ViewModelMapper<VoteViewModel, VoteView> {

    /**
     * Map a [VoteViewModel] instance to a [VoteView] instance
     */
    override fun mapToFromModel(type: VoteViewModel): VoteView {
        return VoteView(type.title, type.description)
    }

    /**
     * Map a [VoteView] instance to a [VoteViewModel] instance
     */
    override fun mapToViewModel(type: VoteView): VoteViewModel {
        return VoteViewModel(type.title, type.description)
    }

}