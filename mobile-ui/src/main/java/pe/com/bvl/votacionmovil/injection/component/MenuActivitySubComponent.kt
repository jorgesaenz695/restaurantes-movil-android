package pe.com.bvl.votacionmovil.injection.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pe.com.bvl.votacionmovil.ui.Fragment.CategoriesFragment
import pe.com.bvl.votacionmovil.ui.activity.MenuActivity

@Subcomponent
interface MenuActivitySubComponent : AndroidInjector<MenuActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MenuActivity>()
}