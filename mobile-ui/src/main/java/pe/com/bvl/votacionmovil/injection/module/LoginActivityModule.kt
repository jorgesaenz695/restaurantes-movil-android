package pe.com.bvl.votacionmovil.injection.module

import dagger.Module
import dagger.Provides
import pe.com.bvl.votacionmovil.domain.interactor.user.LoginUseCase
import pe.com.bvl.votacionmovil.injection.scopes.PerActivity
import pe.com.bvl.votacionmovil.presentation.model.mapper.UserViewMapper
import pe.com.bvl.votacionmovil.presentation.user.LoginContract
import pe.com.bvl.votacionmovil.presentation.user.LoginPresenter
import pe.com.bvl.votacionmovil.ui.activity.LoginActivity


/**
 * Module used to provide dependencies at an activity-level.
 */
@Module
open class LoginActivityModule {

    @PerActivity
    @Provides
    internal fun provideLoginView(loginActivity: LoginActivity): LoginContract.View {
        return loginActivity
    }

    @PerActivity
    @Provides
    internal fun provideLoginPresenter(
        loginView: LoginContract.View,
        login: LoginUseCase,
        mapper: UserViewMapper
    ): LoginContract.Presenter {
        return LoginPresenter(loginView, login, mapper)
    }

}
