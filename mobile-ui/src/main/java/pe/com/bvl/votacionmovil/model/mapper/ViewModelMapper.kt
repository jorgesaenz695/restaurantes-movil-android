package pe.com.bvl.votacionmovil.model.mapper

/**
 * Interface for model mappers. It provides helper methods that facilitate
 * retrieving of models from outer layers
 *
 * @param <V> the view input type
 * @param <D> the view model output type
 */
interface ViewModelMapper<V, D> {

    fun mapToViewModel(type: D): V

    fun mapToFromModel(type: V): D

}