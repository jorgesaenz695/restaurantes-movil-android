package pe.com.bvl.votacionmovil.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_categories.*
import kotlinx.android.synthetic.main.activity_vote.*
import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.model.mapper.CategoriesViewModelMapper
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesContract
import pe.com.bvl.votacionmovil.presentation.model.CategoriesView
import pe.com.bvl.votacionmovil.ui.adapter.CategoriesAdapter
import javax.inject.Inject

class CategoriesActivity : BaseActivity(), GetCategoriesContract.View{


    @Inject
    lateinit var getCategoriesPresenter:GetCategoriesContract.Presenter
    @Inject
    lateinit var categoriesMapper: CategoriesViewModelMapper
    @Inject
    lateinit var categoriesAdapter: CategoriesAdapter

    override fun setPresenter(presenter: GetCategoriesContract.Presenter) {
        getCategoriesPresenter = presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        AndroidInjection.inject(this)
        setUpView()
    }

    private fun setUpView(){
        setupBrowserRecycler()
        getCategoriesPresenter.initialize()
    }

    private fun setupBrowserRecycler(){
        rcvListCategories.layoutManager = LinearLayoutManager(this)
        rcvListCategories.adapter = categoriesAdapter
    }

    override fun showGetCategoriesSuccess(categoriesView: List<CategoriesView>) {
        var categoriesViewModel = categoriesMapper.mapToViewModel(categoriesView)

        categoriesAdapter.categories = categoriesViewModel
        categoriesAdapter.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        getCategoriesPresenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        getCategoriesPresenter.onDestroy()
    }
}
