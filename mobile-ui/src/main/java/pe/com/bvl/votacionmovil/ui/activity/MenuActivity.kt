package pe.com.bvl.votacionmovil.ui.activity

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_menu.*
import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesContract
import pe.com.bvl.votacionmovil.presentation.model.UserView
import pe.com.bvl.votacionmovil.presentation.vote.GetMenuContract
import pe.com.bvl.votacionmovil.ui.Fragment.CategoriesFragment
import pe.com.bvl.votacionmovil.ui.Fragment.RestaurantsFragment

class MenuActivity : BaseActivity(), GetMenuContract.View{

    val BUNDLE_DATA_USER_LOGIN = "BUNDLE_DATA_USER_LOGIN"
    lateinit var FRAGMENT_RESTAURANTS: Fragment
    lateinit var FRAGMENT_CATEGORIES: Fragment
    lateinit var CURRENT_FRAGMENT: Fragment

    var goUserView : UserView? = UserView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        AndroidInjection.inject(this)
        setUpView()
    }

    private fun setUpView(){
        goUserView = intent.getSerializableExtra(BUNDLE_DATA_USER_LOGIN) as? UserView

        //temp
        goUserView = UserView("Jorge","Saenz", "Ugaz")

        instanciateFragments()
        eventControls()

    }

    private fun instanciateFragments(){
        FRAGMENT_RESTAURANTS = RestaurantsFragment.newInstance("","")
        FRAGMENT_CATEGORIES = CategoriesFragment.newInstance(goUserView)
        CURRENT_FRAGMENT = FRAGMENT_RESTAURANTS
    }

    private fun eventControls(){
        supportFragmentManager.beginTransaction().add(R.id.flContent,FRAGMENT_RESTAURANTS,"FRAGMENT_RESTAURANTS").commit()
        supportFragmentManager.beginTransaction().add(R.id.flContent, FRAGMENT_CATEGORIES, "FRAGMENT_CATEGORIES").hide(FRAGMENT_CATEGORIES).commit()

        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                loadFragment(FRAGMENT_RESTAURANTS)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                loadFragment(FRAGMENT_CATEGORIES)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun loadFragment(fragmentChange:Fragment){
        if(CURRENT_FRAGMENT != fragmentChange){
            supportFragmentManager.beginTransaction().hide(CURRENT_FRAGMENT).show(fragmentChange).commit()
            CURRENT_FRAGMENT = fragmentChange
        }
    }

}
