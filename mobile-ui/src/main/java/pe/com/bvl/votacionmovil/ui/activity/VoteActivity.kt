package pe.com.bvl.votacionmovil.ui.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_vote.*
import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.model.mapper.UserViewModelMapper
import pe.com.bvl.votacionmovil.model.mapper.VoteViewModelMapper
import pe.com.bvl.votacionmovil.presentation.model.UserView
import pe.com.bvl.votacionmovil.presentation.model.VoteView
import pe.com.bvl.votacionmovil.presentation.user.GetUserContract
import pe.com.bvl.votacionmovil.presentation.vote.GetVotesContract
import pe.com.bvl.votacionmovil.ui.adapter.VoteAdapter
import javax.inject.Inject

class VoteActivity : BaseActivity(), GetVotesContract.View, GetUserContract.View {


    @Inject
    lateinit var onGetUserPresenter: GetUserContract.Presenter
    @Inject
    lateinit var onGetVotesPresenter: GetVotesContract.Presenter
    @Inject
    lateinit var voteAdapter: VoteAdapter
    @Inject
    lateinit var voteMapper: VoteViewModelMapper
    @Inject
    lateinit var userMapper: UserViewModelMapper


    override fun setPresenter(presenter: GetUserContract.Presenter) {
        onGetUserPresenter = presenter
    }

    override fun setPresenter(presenter: GetVotesContract.Presenter) {
        onGetVotesPresenter = presenter
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote)
        AndroidInjection.inject(this)
        setupBrowseRecycler()

        onGetUserPresenter.initialize()
    }

    private fun setupBrowseRecycler() {
        recycler_vote.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler_vote.adapter = voteAdapter
    }

    override fun showGetUserSuccess(userView: UserView) {
        var userViewModel = userMapper.mapToViewModel(userView)

        Toast.makeText(baseContext, "Bienvenido " + userViewModel.getText(), Toast.LENGTH_LONG).show()
    }

    override fun showGetVotesSuccess(votes: List<VoteView>) {
        voteAdapter.votes = votes.map { voteMapper.mapToViewModel(it) }
        voteAdapter.notifyDataSetChanged()
        recycler_vote.visibility = View.VISIBLE
    }


    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        onGetVotesPresenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        onGetVotesPresenter.onDestroy()
        onGetUserPresenter.onDestroy()
    }


}