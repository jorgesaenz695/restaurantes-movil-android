package pe.com.bvl.votacionmovil.injection

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import pe.com.bvl.votacionmovil.AndroidApplication
import pe.com.bvl.votacionmovil.injection.module.ActivityBindingModule
import pe.com.bvl.votacionmovil.injection.module.ApplicationModule
import pe.com.bvl.votacionmovil.injection.scopes.PerApplication

@PerApplication
@Component(
    modules = arrayOf(
        ActivityBindingModule::class, ApplicationModule::class,
        AndroidSupportInjectionModule::class
    )
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: AndroidApplication)

}
