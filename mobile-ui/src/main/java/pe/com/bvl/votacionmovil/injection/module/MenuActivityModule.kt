package pe.com.bvl.votacionmovil.injection.module

import dagger.Module
import dagger.Provides
import pe.com.bvl.votacionmovil.domain.interactor.categories.GetCategoriesUseCase
import pe.com.bvl.votacionmovil.injection.scopes.PerActivity
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesContract
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesPresenter
import pe.com.bvl.votacionmovil.presentation.model.mapper.CategoriesViewMapper
import pe.com.bvl.votacionmovil.presentation.vote.GetMenuContract
import pe.com.bvl.votacionmovil.ui.activity.CategoriesActivity
import pe.com.bvl.votacionmovil.ui.activity.MenuActivity


/**
 * Module used to provide dependencies at an activity-level.
 */
@Module
open class MenuActivityModule {

    @PerActivity
    @Provides
    internal fun provideGetMenuView(menuActivity: MenuActivity): GetMenuContract.View {
        return menuActivity
    }

}
