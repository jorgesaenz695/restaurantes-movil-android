package pe.com.bvl.votacionmovil.navigation

import android.content.Context
import android.content.Intent
import pe.com.bvl.votacionmovil.ui.activity.LoginActivity
import pe.com.bvl.votacionmovil.ui.activity.VoteActivity

object Navigator {

    fun navigateToLogin(context: Context) {
        if (context != null) {
            var intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    fun navigateToVote(context: Context) {
        if (context != null) {
            var intent = Intent(context, VoteActivity::class.java)
            context.startActivity(intent)
        }
    }
}