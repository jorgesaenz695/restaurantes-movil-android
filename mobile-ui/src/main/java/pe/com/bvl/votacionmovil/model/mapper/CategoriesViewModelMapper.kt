package pe.com.bvl.votacionmovil.model.mapper

import pe.com.bvl.votacionmovil.model.CategoriesViewModel
import pe.com.bvl.votacionmovil.presentation.model.CategoriesView
import javax.inject.Inject

/**
 * Map a [CategoriesViewModel] to and from a [CategoriesView] instance when data is moving between
 * this later and the Data layer
 */
open class CategoriesViewModelMapper @Inject constructor() :
    ViewModelMapper<List<CategoriesViewModel>, List<CategoriesView>> {

    /**
     * Map an instance of a [CategoriesViewModel] to a [CategoriesView] model
     */
    override fun mapToFromModel(type: List<CategoriesViewModel>): List<CategoriesView> {

        val list = ArrayList<CategoriesView>()

        for(categories in type){
            list.add(CategoriesView(categories.id, categories.name, categories.description))
        }

        return list
    }

    override fun mapToViewModel(type: List<CategoriesView>): List<CategoriesViewModel> {

        val list = ArrayList<CategoriesViewModel>()

        for(categories in type){
            list.add(CategoriesViewModel(categories.id, categories.name, categories.description))
        }

        return list
    }

}