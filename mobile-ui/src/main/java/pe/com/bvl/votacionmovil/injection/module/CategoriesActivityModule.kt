package pe.com.bvl.votacionmovil.injection.module

import dagger.Module
import dagger.Provides
import pe.com.bvl.votacionmovil.domain.interactor.categories.GetCategoriesUseCase
import pe.com.bvl.votacionmovil.injection.scopes.PerActivity
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesContract
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesPresenter
import pe.com.bvl.votacionmovil.presentation.model.mapper.CategoriesViewMapper
import pe.com.bvl.votacionmovil.ui.Fragment.CategoriesFragment
import pe.com.bvl.votacionmovil.ui.activity.CategoriesActivity


/**
 * Module used to provide dependencies at an activity-level.
 */
@Module
open class CategoriesActivityModule {

    @PerActivity
    @Provides
    internal fun provideCategoriesView(CategoriesActivity: CategoriesActivity): GetCategoriesContract.View {
        return CategoriesActivity
    }

    @PerActivity
    @Provides
    internal fun provideCategoriesPresenter(
        categoriesView: GetCategoriesContract.View,
        categories: GetCategoriesUseCase,
        mapper: CategoriesViewMapper
    ): GetCategoriesContract.Presenter {
        return GetCategoriesPresenter(categoriesView, categories, mapper)
    }

}
