package pe.com.bvl.votacionmovil.injection.module

import dagger.Module
import dagger.Provides
import pe.com.bvl.votacionmovil.domain.interactor.user.GetUserUseCase
import pe.com.bvl.votacionmovil.domain.interactor.vote.GetVotesUseCase
import pe.com.bvl.votacionmovil.injection.scopes.PerActivity
import pe.com.bvl.votacionmovil.presentation.model.mapper.UserViewMapper
import pe.com.bvl.votacionmovil.presentation.model.mapper.VoteViewMapper
import pe.com.bvl.votacionmovil.presentation.user.GetUserContract
import pe.com.bvl.votacionmovil.presentation.user.GetUserPresenter
import pe.com.bvl.votacionmovil.presentation.vote.GetVotesContract
import pe.com.bvl.votacionmovil.presentation.vote.GetVotesPresenter
import pe.com.bvl.votacionmovil.ui.activity.VoteActivity


/**
 * Module used to provide dependencies at an activity-level.
 */
@Module
open class VoteActivityModule {

    @PerActivity
    @Provides
    internal fun provideGetVotesView(votesActivity: VoteActivity): GetVotesContract.View {
        return votesActivity
    }


    @PerActivity
    @Provides
    internal fun provideGetVotesPresenter(
        view: GetVotesContract.View,
        userCase: GetVotesUseCase,
        mapper: VoteViewMapper
    ): GetVotesContract.Presenter {
        return GetVotesPresenter(view, userCase, mapper)
    }

    @PerActivity
    @Provides
    internal fun provideGetUserView(votesActivity: VoteActivity): GetUserContract.View {
        return votesActivity
    }

    @PerActivity
    @Provides
    internal fun provideGetUserPresenter(
        view: GetUserContract.View,
        userCase: GetUserUseCase,
        mapper: UserViewMapper
    ): GetUserContract.Presenter {
        return GetUserPresenter(view, userCase, mapper)
    }

}
