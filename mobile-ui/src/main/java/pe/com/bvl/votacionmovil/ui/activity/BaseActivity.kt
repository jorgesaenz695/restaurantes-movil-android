package pe.com.bvl.votacionmovil.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    open fun showProgress() {

    }

    open fun hideProgress() {

    }

    open fun showError(error: Throwable) {
        Toast.makeText(this,error.toString(),LENGTH_LONG).show()
    }

    open fun hideError() {

    }

}