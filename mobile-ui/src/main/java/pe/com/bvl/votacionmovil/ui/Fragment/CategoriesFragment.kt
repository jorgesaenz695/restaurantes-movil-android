package pe.com.bvl.votacionmovil.ui.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_categories.*

import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.model.mapper.CategoriesViewModelMapper
import pe.com.bvl.votacionmovil.presentation.categories.GetCategoriesContract
import pe.com.bvl.votacionmovil.presentation.model.CategoriesView
import pe.com.bvl.votacionmovil.presentation.model.UserView
import pe.com.bvl.votacionmovil.ui.adapter.CategoriesAdapter
import javax.inject.Inject
import android.content.Context


class CategoriesFragment : Fragment(), GetCategoriesContract.View {

    private val BUNDLE_DATA_USER_LOGIN = "BUNDLE_DATA_USER_LOGIN"
    var userView: UserView? = UserView()

    @Inject
    lateinit var getCategoriesPresenter:GetCategoriesContract.Presenter
    @Inject
    lateinit var categoriesMapper: CategoriesViewModelMapper
    @Inject
    lateinit var categoriesAdapter: CategoriesAdapter

    override fun setPresenter(presenter: GetCategoriesContract.Presenter) {
        getCategoriesPresenter = presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpView()
    }

    private fun setUpView(){
        getBundlesBeginOpFrequent()
        setupBrowsrRecycler()
    }

    private fun getBundlesBeginOpFrequent(){
        userView = arguments!!.getSerializable(BUNDLE_DATA_USER_LOGIN) as UserView
    }

    private fun setupBrowsrRecycler(){
        rcvListCategories.layoutManager = LinearLayoutManager(context)
        rcvListCategories.adapter = categoriesAdapter
    }

    override fun showGetCategoriesSuccess(categoriesView: List<CategoriesView>) {
        var categoriesViewModel = categoriesMapper.mapToViewModel(categoriesView)

        categoriesAdapter.categories = categoriesViewModel
        categoriesAdapter.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        getCategoriesPresenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        getCategoriesPresenter.onDestroy()
    }

    companion object {
        @JvmStatic
        fun newInstance(userView: UserView?) =
            CategoriesFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BUNDLE_DATA_USER_LOGIN, userView)
                }
            }
    }



    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showError(error: Throwable) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
