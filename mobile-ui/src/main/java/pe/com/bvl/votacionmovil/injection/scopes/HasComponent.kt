package pe.com.bvl.votacionmovil.injection.scopes

interface HasComponent<C> {
    val component: C
}
