package pe.com.bvl.votacionmovil.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_category.view.*
import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.model.CategoriesViewModel
import javax.inject.Inject

class CategoriesAdapter @Inject constructor(): RecyclerView.Adapter<CategoriesAdapter.ViewHolder>(){

    var categories: List<CategoriesViewModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = categories[position]
        holder.txtTittle.text = category.name
        holder.txtDescription.text = category.description
    }


    override fun getItemCount(): Int {
        return categories.size
    }

    inner class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        var txtDescription : TextView
        var txtTittle : TextView

        init {
            txtDescription = view.text_description
            txtTittle  = view.text_title
        }

    }


}