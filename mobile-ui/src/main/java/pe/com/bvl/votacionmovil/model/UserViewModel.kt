package pe.com.bvl.votacionmovil.model

/**
 * Representation for a [UserViewModel] fetched from an external layer data source
 */
class UserViewModel(val name: String? = null, val lastName: String? = null, val motherLastName: String? = null) {

    fun getText(): String {
        return "$name $lastName $motherLastName"
    }
}