package pe.com.bvl.votacionmovil.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.model.VoteViewModel
import javax.inject.Inject

class VoteAdapter @Inject constructor() : androidx.recyclerview.widget.RecyclerView.Adapter<VoteAdapter.ViewHolder>() {

    var votes: List<VoteViewModel> = arrayListOf()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vote = votes[position]
        holder.descriptionText.text = vote.description
        holder.titleText.text = vote.title
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_vote, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return votes.size
    }

    inner class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

        var descriptionText: TextView
        var titleText: TextView

        init {
            descriptionText = view.findViewById(R.id.text_decription)
            titleText = view.findViewById(R.id.text_title)
        }
    }

}