package pe.com.bvl.votacionmovil.model

class CategoriesViewModel (
    var id: Int? =null,
    var name: String? = null,
    var description: String? = null
)