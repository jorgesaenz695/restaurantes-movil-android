package pe.com.bvl.votacionmovil.injection.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import pe.com.bvl.votacionmovil.BuildConfig
import pe.com.bvl.votacionmovil.UiThread
import pe.com.bvl.votacionmovil.cache.PreferencesHelper
import pe.com.bvl.votacionmovil.cache.UserCacheImpl
import pe.com.bvl.votacionmovil.cache.VoteCacheImpl
import pe.com.bvl.votacionmovil.cache.dto.mapper.UserCacheMapper
import pe.com.bvl.votacionmovil.cache.dto.mapper.VoteCacheMapper
import pe.com.bvl.votacionmovil.data.CategoriesDataRepository
import pe.com.bvl.votacionmovil.data.UserDataRepository
import pe.com.bvl.votacionmovil.data.VoteDataRepository
import pe.com.bvl.votacionmovil.data.executor.JobExecutor
import pe.com.bvl.votacionmovil.data.model.mapper.CategoriesMapper
import pe.com.bvl.votacionmovil.data.model.mapper.UserMapper
import pe.com.bvl.votacionmovil.data.model.mapper.VoteMapper
import pe.com.bvl.votacionmovil.data.repository.*
import pe.com.bvl.votacionmovil.data.source.CategoriesDataStoreFactory
import pe.com.bvl.votacionmovil.data.source.UserDataStoreFactory
import pe.com.bvl.votacionmovil.data.source.VoteDataStoreFactory
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.domain.executor.PostExecutionThread
import pe.com.bvl.votacionmovil.domain.executor.ThreadExecutor
import pe.com.bvl.votacionmovil.domain.repository.CategoriesRepository
import pe.com.bvl.votacionmovil.domain.repository.UserRepository
import pe.com.bvl.votacionmovil.domain.repository.VoteRepository
import pe.com.bvl.votacionmovil.injection.scopes.PerApplication
import pe.com.bvl.votacionmovil.remote.*
import pe.com.bvl.votacionmovil.remote.model.mapper.CategoriesRemMapper
import pe.com.bvl.votacionmovil.remote.model.mapper.RequestLoginRemMapper
import pe.com.bvl.votacionmovil.remote.model.mapper.UserRemMapper
import pe.com.bvl.votacionmovil.remote.model.mapper.VoteRemMapper
import pe.com.bvl.votacionmovil.util.CommonUtilImpl

/**
 * Module used to provide dependencies at an application-level.
 */
@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }


    @Provides
    @PerApplication
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }


    @Provides
    @PerApplication
    internal fun provideCommonUtil(context: Context): CommonUtil {
        return CommonUtilImpl(context)
    }

    @Provides
    @PerApplication
    internal fun providePreferencesHelper(context: Context): PreferencesHelper {
        return PreferencesHelper(context)
    }

    /*** INICIO USER ***/


    @Provides
    @PerApplication
    internal fun provideUserRepository(
        factory: UserDataStoreFactory,
        mapper: UserMapper
    ): UserRepository {
        return UserDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideUserCache(
        remoteMapper: UserCacheMapper
    ): UserCache {
        return UserCacheImpl(remoteMapper)
    }


    @Provides
    @PerApplication
    internal fun provideUserRemote(
        service: UserService,
        userMapper: UserRemMapper,
        loginRemMapper: RequestLoginRemMapper,
        commonUtil: CommonUtil
    ): UserRemote {
        return UserRemoteImpl(service, userMapper, loginRemMapper, commonUtil)
    }


    @Provides
    @PerApplication
    internal fun provideUserService(): UserService {
        return ServiceFactory.makeUserService(BuildConfig.DEBUG)
    }


    /*** FIN USER ***/

    /*** INICIO VOTE ***/


    @Provides
    @PerApplication
    internal fun provideVoteRepository(
        factory: VoteDataStoreFactory,
        mapper: VoteMapper
    ): VoteRepository {
        return VoteDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideDataCache(
        cacheMapper: VoteCacheMapper,
        preferencesHelper: PreferencesHelper
    ): VoteCache {
        return VoteCacheImpl(cacheMapper, preferencesHelper)
    }


    @Provides
    @PerApplication
    internal fun provideVoteRemote(
        service: VoteService,
        remMapper: VoteRemMapper,
        commonUtil: CommonUtil
    ): VoteRemote {
        return VoteRemoteImpl(service, remMapper, commonUtil)
    }


    @Provides
    @PerApplication
    internal fun provideVoteService(): VoteService {
        return ServiceFactory.makeVoteService(BuildConfig.DEBUG)
    }


    /*** FIN VOTE ***/

    /*** INICIO Categories ***/


    @Provides
    @PerApplication
    internal fun provideCategoriesRepository(
        factory: CategoriesDataStoreFactory,
        mapper: CategoriesMapper
    ): CategoriesRepository {
        return CategoriesDataRepository(factory, mapper)
    }

//    @Provides
//    @PerApplication
//    internal fun provideDataCache(
//        cacheMapper: CategoriesCacheMapper,
//        preferencesHelper: PreferencesHelper
//    ): CategoriesCache {
//        return CategoriesCacheImpl(cacheMapper, preferencesHelper)
//    }


    @Provides
    @PerApplication
    internal fun provideCategoriesRemote(
        service: CategoriesService,
        remMapper: CategoriesRemMapper,
        commonUtil: CommonUtil
    ): CategoriesRemote {
        return CategoriesRemoteImpl(service, remMapper, commonUtil)
    }


    @Provides
    @PerApplication
    internal fun provideCategoriesService(): CategoriesService {
        return ServiceFactory.makeCategoriesService(BuildConfig.DEBUG)
    }


    /*** FIN Categories ***/


}
