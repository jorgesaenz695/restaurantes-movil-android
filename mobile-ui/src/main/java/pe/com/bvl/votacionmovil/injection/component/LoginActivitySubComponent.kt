package pe.com.bvl.votacionmovil.injection.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pe.com.bvl.votacionmovil.ui.activity.LoginActivity

@Subcomponent
interface LoginActivitySubComponent : AndroidInjector<LoginActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<LoginActivity>()

}