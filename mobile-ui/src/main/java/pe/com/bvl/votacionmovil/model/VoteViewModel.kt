package pe.com.bvl.votacionmovil.model


/**
 * Representation for a [VoteViewModel] fetched from an external layer data source
 */
class VoteViewModel(
    var title: String? = null,
    var description: String? = null
)