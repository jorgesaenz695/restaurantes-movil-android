package pe.com.bvl.votacionmovil.injection.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pe.com.bvl.votacionmovil.injection.scopes.PerActivity
import pe.com.bvl.votacionmovil.ui.Fragment.CategoriesFragment
import pe.com.bvl.votacionmovil.ui.activity.CategoriesActivity
import pe.com.bvl.votacionmovil.ui.activity.LoginActivity
import pe.com.bvl.votacionmovil.ui.activity.MenuActivity
import pe.com.bvl.votacionmovil.ui.activity.VoteActivity

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(LoginActivityModule::class))
    abstract fun bindLoginActivity(): LoginActivity

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(VoteActivityModule::class))
    abstract fun bindVoteActivity(): VoteActivity

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(CategoriesActivityModule::class))
    abstract fun bindCategoriesActivity(): CategoriesActivity

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(MenuActivityModule::class))
    abstract fun bindMenuActivity(): MenuActivity
}