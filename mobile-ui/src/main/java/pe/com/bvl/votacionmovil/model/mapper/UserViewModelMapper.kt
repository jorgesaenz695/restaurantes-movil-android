package pe.com.bvl.votacionmovil.model.mapper

import pe.com.bvl.votacionmovil.model.UserViewModel
import pe.com.bvl.votacionmovil.presentation.model.UserView
import javax.inject.Inject

/**
 * Map a [UserView] to and from a [UserViewModel] instance when data is moving between
 * this layer and the Domain layer
 */
open class UserViewModelMapper @Inject constructor() : ViewModelMapper<UserViewModel, UserView> {

    /**
     * Map a [UserViewModel] instance to a [UserView] instance
     */
    override fun mapToFromModel(type: UserViewModel): UserView {
        return UserView(type.name, type.lastName, type.motherLastName)
    }

    /**
     * Map a [UserView] instance to a [UserViewModel] instance
     */
    override fun mapToViewModel(type: UserView): UserViewModel {
        return UserViewModel(type.name, type.lastName, type.motherLastName)
    }

}