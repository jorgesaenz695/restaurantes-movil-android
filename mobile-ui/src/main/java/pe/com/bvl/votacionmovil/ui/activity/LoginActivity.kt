package pe.com.bvl.votacionmovil.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import pe.com.bvl.votacionmovil.presentation.model.UserView
import pe.com.bvl.votacionmovil.model.mapper.UserViewModelMapper
import pe.com.bvl.votacionmovil.R
import pe.com.bvl.votacionmovil.navigation.Navigator
import pe.com.bvl.votacionmovil.presentation.user.LoginContract
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract.View, View.OnClickListener {


    @Inject
    lateinit var loginPresenter: LoginContract.Presenter
    @Inject
    lateinit var mapper: UserViewModelMapper


    override fun setPresenter(presenter: LoginContract.Presenter) {
        loginPresenter = presenter
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_login)
        AndroidInjection.inject(this)
        btnSend.setOnClickListener(this)

    }


    override fun showLoginSuccess(userView: UserView) {
        Navigator.navigateToVote(this)
        finish()
    }

    override fun onStart() {
        super.onStart()
        loginPresenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        loginPresenter.onDestroy()
    }

    override fun onClick(v: View?) {
        when (v) {
            btnSend -> {
                var email = edtEmail.text.toString()
                var password = edtPassword.text.toString()
                loginPresenter.initialize(email, password)
            }
        }
    }

}