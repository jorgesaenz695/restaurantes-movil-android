package pe.com.bvl.votacionmovil.util

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import pe.com.bvl.votacionmovil.BuildConfig
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Manuel Torres on 5/01/2017.
 */
@Singleton
class CommonUtilImpl @Inject constructor(var context: Context) : CommonUtil {

    /**
     * Tag used on log messages.
     */
    internal val TAG = CommonUtilImpl::class.java.simpleName
    private val showLog = false

    override fun isOnline(): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }


    override fun logV(psTAG: String, psMessage: String) {
        var psTAG = psTAG
        if (BuildConfig.DEBUG || showLog) {
            if (showLog) {
                psTAG = "Pagalo-$psTAG"
            }
            Log.v(psTAG, psMessage)
        }
    }

    override fun logD(psTAG: String, psMessage: String) {
        var psTAG = psTAG
        if (BuildConfig.DEBUG || showLog) {
            if (showLog) {
                psTAG = "Pagalo-$psTAG"
            }
            Log.d(psTAG, psMessage)
        }
    }

    override fun logI(psTAG: String, psMessage: String) {
        var psTAG = psTAG
        if (BuildConfig.DEBUG || showLog) {
            if (showLog) {
                psTAG = "Pagalo-$psTAG"
            }
            Log.i(psTAG, psMessage)
        }
    }

    override fun logW(psTAG: String, psMessage: String) {
        var psTAG = psTAG
        if (BuildConfig.DEBUG || showLog) {
            if (showLog) {
                psTAG = "Pagalo-$psTAG"
            }
            Log.w(psTAG, psMessage)
        }
    }

    override fun logE(psTAG: String, psMessage: String) {
        var psTAG = psTAG
        if (BuildConfig.DEBUG || showLog) {
            if (showLog) {
                psTAG = "Pagalo-$psTAG"
            }
            Log.e(psTAG, psMessage)
        }
    }

    override fun logE(psTAG: String, psMessage: String, poException: Throwable) {
        var psTAG = psTAG
        if (BuildConfig.DEBUG || showLog) {
            if (showLog) {
                psTAG = "Pagalo-$psTAG"
            }
            Log.e(psTAG, psMessage, poException)
        }
    }

}
