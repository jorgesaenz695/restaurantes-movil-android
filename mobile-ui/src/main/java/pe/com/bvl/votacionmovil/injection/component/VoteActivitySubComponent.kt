package pe.com.bvl.votacionmovil.injection.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pe.com.bvl.votacionmovil.ui.activity.VoteActivity

@Subcomponent
interface VoteActivitySubComponent : AndroidInjector<VoteActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<VoteActivity>()

}