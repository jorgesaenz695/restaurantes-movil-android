package pe.com.bvl.votacionmovil.injection.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pe.com.bvl.votacionmovil.ui.activity.CategoriesActivity

@Subcomponent
interface CategoriesActivitySubComponent : AndroidInjector<CategoriesActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CategoriesActivity>()

}