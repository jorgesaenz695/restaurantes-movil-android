package pe.com.bvl.votacionmovil.data.source

import pe.com.bvl.votacionmovil.data.repository.VoteCache
import pe.com.bvl.votacionmovil.data.repository.VoteDataStore
import javax.inject.Inject

/**
 * Create an instance of a VoteDataStore
 */
open class VoteDataStoreFactory @Inject constructor(
    private val voteCache: VoteCache,
    private val voteCacheDataStore: VoteCacheDataStore,
    private val voteRemoteDataStore: VoteRemoteDataStore
) {

    /**
     * Returns a DataStore based on whether or not there is content in the cache and the cache
     * has not expired
     */
    open fun retrieveDataStore(): VoteDataStore {
        if (voteCache.isCached() && !voteCache.isExpired()) {
            return retrieveCacheDataStore()
        }
        return retrieveRemoteDataStore()
    }

    /**
     * Return an instance of the Remote Data Store
     */
    open fun retrieveCacheDataStore(): VoteDataStore {
        return voteCacheDataStore
    }

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveRemoteDataStore(): VoteDataStore {
        return voteRemoteDataStore
    }

}