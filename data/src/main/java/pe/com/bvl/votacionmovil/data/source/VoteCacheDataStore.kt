package pe.com.bvl.votacionmovil.data.source

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.data.repository.VoteCache
import pe.com.bvl.votacionmovil.data.repository.VoteDataStore
import javax.inject.Inject

/**
 * Implementation of the [VoteDataStore] interface to provide a means of communicating
 * with the local data source
 */
open class VoteCacheDataStore @Inject constructor(private val voteCache: VoteCache) :
    VoteDataStore {

    /**
     * Clear all Votes from the cache
     */
    override fun clearVotes(): Completable {
        return voteCache.clearVotes()
    }

    /**
     * Save a given [List] of [VoteEntity] instances to the cache
     */
    override fun saveVotes(votes: List<VoteEntity>): Completable {
        return voteCache.saveVotes(votes)
            .doOnComplete {
                voteCache.setLastCacheTime(System.currentTimeMillis())
            }
    }

    /**
     * Retrieve a list of [VoteEntity] instance from the cache
     */
    override fun getVotes(): Single<List<VoteEntity>> {
        return voteCache.getVotes()
    }

}