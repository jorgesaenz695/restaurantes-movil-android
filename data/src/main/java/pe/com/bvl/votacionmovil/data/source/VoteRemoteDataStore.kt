package pe.com.bvl.votacionmovil.data.source

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.data.repository.VoteDataStore
import pe.com.bvl.votacionmovil.data.repository.VoteRemote
import javax.inject.Inject

/**
 * Implementation of the [VoteDataStore] interface to provide a means of communicating
 * with the remote data source
 */
open class VoteRemoteDataStore @Inject constructor(private val voteRemote: VoteRemote) :
    VoteDataStore {


    override fun clearVotes(): Completable {
        throw UnsupportedOperationException()
    }

    override fun saveVotes(votes: List<VoteEntity>): Completable {
        throw UnsupportedOperationException()
    }

    /**
     * Retrieve a list of [VoteEntity] instances from the API
     */
    override fun getVotes(): Single<List<VoteEntity>> {

        return voteRemote.getVotes()
    }

}