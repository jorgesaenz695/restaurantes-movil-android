package pe.com.bvl.votacionmovil.data.exception

/**
 * Created by Manuel Torres on 5/01/2017.
 */

class BaseException : Exception {

    companion object {
        val OK = "00000"
        val ERROR_GENERAL = "99999"
        val ERROR_DATA_RESPONSE = "99998"
        val CONNECTION_SERVER = "99997"
        val ERROR_CONNECTION = "99996"
        val ERROR_SESSION_USER = "99995"
        val ERROR_BD_USER_NOT_EXIST = "99995"
        val ERROR_SESSION_SERVICE = "60000"
        val ERROR_TOKEN_NOT_EXIST_SERVICE = "60001"
    }

    var codeError: String? = null
    var gsMessage: String? = null
    var messageError: String? = null


    constructor(psCodeError: String, psMessage: String, poCause: Throwable) : super(poCause) {
        this.codeError = psCodeError
        this.gsMessage = psMessage
    }

    constructor(psCodeError: String, psMessage: String, psMessageError: String, poCause: Throwable) : super(poCause) {
        this.codeError = psCodeError
        this.gsMessage = psMessage
        this.messageError = psMessageError
    }

    constructor(psCodeError: String, psMessage: String, psMessageError: String) : super() {
        this.codeError = psCodeError
        this.gsMessage = psMessage
        this.messageError = psMessageError
    }

    constructor(psCodeError: String, psMessage: String) : super() {
        this.codeError = psCodeError
        this.gsMessage = psMessage
    }


}
