package pe.com.bvl.votacionmovil.data.model


/**
 * Representation for a [VoteRem] fetched from the API
 */
class VoteEntity(
    var title: String? = null,
    var description: String? = null
)