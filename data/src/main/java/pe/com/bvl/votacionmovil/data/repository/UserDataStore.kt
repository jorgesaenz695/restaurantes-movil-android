package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity

/**
 * Interface defining methods for the data operations related to User.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented
 */
interface UserDataStore {

    fun login(requestLoginEntity: RequestLoginEntity): Single<UserEntity>

    fun getUser(): Single<UserEntity>

    fun saveUser(userEntity: UserEntity): Completable

}