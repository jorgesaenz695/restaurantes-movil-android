package pe.com.bvl.votacionmovil.data

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.mapper.CategoriesMapper
import pe.com.bvl.votacionmovil.data.source.CategoriesDataStoreFactory
import pe.com.bvl.votacionmovil.domain.model.Categories
import pe.com.bvl.votacionmovil.domain.repository.CategoriesRepository
import pe.com.bvl.votacionmovil.domain.repository.UserRepository
import javax.inject.Inject

/**
 * Provides an implementation of the [CategoriesDataRepository] interface for communicating to and from
 * data sources
 */

class CategoriesDataRepository @Inject constructor(
    private val factory:CategoriesDataStoreFactory,
    private val categoriesMapper: CategoriesMapper
)
    : CategoriesRepository {
    
    override fun categories(): Single<List<Categories>> {
        return factory.retrieveRemoteDataStore().getCategories().map {
            categoriesMapper.mapFromEntity(it)
        }
    }

}