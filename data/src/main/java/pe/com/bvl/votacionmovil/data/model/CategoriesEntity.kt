package pe.com.bvl.votacionmovil.data.model

class CategoriesEntity (
    var id: Int? =null,
    var name: String? = null,
    var description: String? = null
)