package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.VoteEntity

interface VoteCache {

    fun clearVotes(): Completable

    fun saveVotes(votes: List<VoteEntity>): Completable

    fun getVotes(): Single<List<VoteEntity>>

    fun isCached(): Boolean

    fun setLastCacheTime(lastCache: Long)

    fun isExpired(): Boolean

}