package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.CategoriesEntity

/**
 * Interface defining methods for the data operations related to Categories.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented
 */
interface CategoriesDataStore {

    fun getCategories(): Single<List<CategoriesEntity>>

}