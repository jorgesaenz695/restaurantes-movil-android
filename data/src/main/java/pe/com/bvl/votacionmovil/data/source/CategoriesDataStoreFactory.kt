package pe.com.bvl.votacionmovil.data.source

import pe.com.bvl.votacionmovil.data.repository.CategoriesDataStore
import javax.inject.Inject

/**
 * Create an instance of a CategoriesDataStore
 */
open class CategoriesDataStoreFactory @Inject constructor(
    private val categoriesRemoteDataStore: CategoriesRemoteDataStore
) {

    /**
     * Return an instance of the Remote Data Store
     */
    open fun retrieveRemoteDataStore(): CategoriesDataStore {
        return categoriesRemoteDataStore
    }

}