package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.model.VoteEntity

/**
 * Interface defining methods for the data operations related to Vote.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented
 */
interface VoteDataStore {

    fun clearVotes(): Completable

    fun saveVotes(votes: List<VoteEntity>): Completable

    fun getVotes(): Single<List<VoteEntity>>

}