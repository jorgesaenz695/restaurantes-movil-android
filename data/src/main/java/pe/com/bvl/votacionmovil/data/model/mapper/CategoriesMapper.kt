package pe.com.bvl.votacionmovil.data.model.mapper

import pe.com.bvl.votacionmovil.data.model.CategoriesEntity
import pe.com.bvl.votacionmovil.domain.model.Categories
import javax.inject.Inject

/**
 * Map a [CategoriesEntity] to and from a [Categories] instance when data is moving between
 * this later and the Data layer
 */
open class CategoriesMapper @Inject constructor() : Mapper<List<CategoriesEntity>, List<Categories>> {

    /**
     * Map an instance of a [CategoriesRem] to a [CategoriesEntity] model
     */

    override fun mapFromEntity(type: List<CategoriesEntity>): List<Categories> {
        val list = ArrayList<Categories>()

        for(categories in type){
            list.add(Categories(categories.id, categories.name, categories.description))
        }

        return list
    }

    override fun mapToEntity(type: List<Categories>): List<CategoriesEntity> {
        val list = ArrayList<CategoriesEntity>()

        for(categories in type){
            list.add(CategoriesEntity(categories.id, categories.name, categories.description))
        }

        return list
    }
}