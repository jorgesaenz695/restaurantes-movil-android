package pe.com.bvl.votacionmovil.data.model.mapper

import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.domain.model.Vote
import javax.inject.Inject


/**
 * Map a [VoteEntity] to and from a [Vote] instance when data is moving between
 * this later and the Domain layer
 */
open class VoteMapper @Inject constructor() : Mapper<VoteEntity, Vote> {

    /**
     * Map a [VoteEntity] instance to a [Vote] instance
     */
    override fun mapFromEntity(type: VoteEntity): Vote {
        return Vote(type.title, type.description)
    }

    /**
     * Map a [Vote] instance to a [VoteEntity] instance
     */
    override fun mapToEntity(type: Vote): VoteEntity {
        return VoteEntity(type.title, type.description)
    }


}