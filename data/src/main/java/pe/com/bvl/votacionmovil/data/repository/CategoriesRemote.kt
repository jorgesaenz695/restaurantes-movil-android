package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.CategoriesEntity
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity


interface CategoriesRemote {

    fun categories(): Single<List<CategoriesEntity>>

}