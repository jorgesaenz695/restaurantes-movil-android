package pe.com.bvl.votacionmovil.data.source

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.repository.UserCache
import pe.com.bvl.votacionmovil.data.repository.UserDataStore
import javax.inject.Inject

/**
 * Implementation of the [UserDataStore] interface to provide a means of communicating
 * with the remote data source
 */
open class UserCacheDataStore @Inject constructor(private val userCache: UserCache) : UserDataStore {


    override fun saveUser(userEntity: UserEntity): Completable {
        return userCache.saveUser(userEntity)
    }


    override fun login(requestLoginEntity: RequestLoginEntity): Single<UserEntity> {
        throw UnsupportedOperationException()
    }

    override fun getUser(): Single<UserEntity> {
        return userCache.getUser()
    }
}

