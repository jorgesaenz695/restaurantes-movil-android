package pe.com.bvl.votacionmovil.data.source

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.repository.UserDataStore
import pe.com.bvl.votacionmovil.data.repository.UserRemote
import javax.inject.Inject

/**
 * Implementation of the [UserDataStore] interface to provide a means of communicating
 * with the remote data source
 */
open class UserRemoteDataStore @Inject constructor(private val userRemote: UserRemote) :
    UserDataStore {


    override fun saveUser(userEntity: UserEntity): Completable {
        throw UnsupportedOperationException()
    }

    override fun getUser(): Single<UserEntity> {
        throw UnsupportedOperationException()
    }

    override fun login(requestLoginEntity: RequestLoginEntity): Single<UserEntity> {
        return userRemote.login(requestLoginEntity)
    }

}