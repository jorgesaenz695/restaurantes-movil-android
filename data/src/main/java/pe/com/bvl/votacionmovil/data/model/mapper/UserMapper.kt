package pe.com.bvl.votacionmovil.data.model.mapper

import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.domain.model.User
import javax.inject.Inject


/**
 * Map a [UserEntity] to and from a [User] instance when data is moving between
 * this later and the Domain layer
 */
open class UserMapper @Inject constructor() : Mapper<UserEntity, User> {

    /**
     * Map a [UserEntity] instance to a [User] instance
     */
    override fun mapFromEntity(type: UserEntity): User {
        return User(type.name, type.lastName, type.motherLastName)
    }

    /**
     * Map a [User] instance to a [UserEntity] instance
     */
    override fun mapToEntity(type: User): UserEntity {
        return UserEntity(type.name, type.lastName, type.motherLastName)
    }


}