package pe.com.bvl.votacionmovil.data

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.data.model.mapper.VoteMapper
import pe.com.bvl.votacionmovil.data.source.VoteDataStoreFactory
import pe.com.bvl.votacionmovil.data.source.VoteRemoteDataStore
import pe.com.bvl.votacionmovil.domain.model.Vote
import pe.com.bvl.votacionmovil.domain.repository.VoteRepository
import javax.inject.Inject

/**
 * Provides an implementation of the [VoteRepository] interface for communicating to and from
 * data sources
 */
class VoteDataRepository @Inject constructor(
    private val factory: VoteDataStoreFactory,
    private val voteMapper: VoteMapper
) :
    VoteRepository {

    override fun clearVotes(): Completable {
        return factory.retrieveCacheDataStore().clearVotes()
    }

    override fun saveVotes(votes: List<Vote>): Completable {
        val voteEntities = votes.map { voteMapper.mapToEntity(it) }
        return saveVoteEntities(voteEntities)
    }

    private fun saveVoteEntities(votes: List<VoteEntity>): Completable {
        return factory.retrieveCacheDataStore().saveVotes(votes)
    }

    override fun getVotes(): Single<List<Vote>> {
        val dataStore = factory.retrieveDataStore()
        return dataStore.getVotes()
            .flatMap {
                if (dataStore is VoteRemoteDataStore) {
                    saveVoteEntities(it).toSingle { it }
                } else {
                    Single.just(it)
                }
            }
            .map { list ->
                list.map { listItem ->
                    voteMapper.mapFromEntity(listItem)
                }
            }
    }

}