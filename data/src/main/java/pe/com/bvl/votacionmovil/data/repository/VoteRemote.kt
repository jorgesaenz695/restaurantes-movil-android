package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.VoteEntity


interface VoteRemote {


    fun getVotes(): Single<List<VoteEntity>>


}