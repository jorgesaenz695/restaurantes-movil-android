package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.UserEntity


interface UserCache {


    fun saveUser(userEntity: UserEntity): Completable

    fun getUser(): Single<UserEntity>

    fun clearUser(): Completable


}