package pe.com.bvl.votacionmovil.data.source

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.CategoriesEntity
import pe.com.bvl.votacionmovil.data.repository.CategoriesDataStore
import pe.com.bvl.votacionmovil.data.repository.CategoriesRemote
import javax.inject.Inject

/**
 * Implementation of the [CategoriesRemoteDataStore] interface to provide a means of communicating
 * with the remote data source
 */
open class CategoriesRemoteDataStore @Inject constructor(private val categoriesRemote: CategoriesRemote) :
    CategoriesDataStore {

    override fun getCategories(): Single<List<CategoriesEntity>> {
        return categoriesRemote.categories()
    }
}