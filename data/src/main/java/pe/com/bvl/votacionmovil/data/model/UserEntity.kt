package pe.com.bvl.votacionmovil.data.model


class UserEntity(val name: String? = null, val lastName: String? = null, val motherLastName: String? = null)