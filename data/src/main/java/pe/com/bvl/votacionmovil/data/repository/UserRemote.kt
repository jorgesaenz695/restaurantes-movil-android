package pe.com.bvl.votacionmovil.data.repository

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity


interface UserRemote {


    fun login(requestLoginEntity: RequestLoginEntity): Single<UserEntity>


}