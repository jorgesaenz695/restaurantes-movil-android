package pe.com.bvl.votacionmovil.data.source

import pe.com.bvl.votacionmovil.data.repository.UserDataStore
import javax.inject.Inject

/**
 * Create an instance of a UserDataStore
 */
open class UserDataStoreFactory @Inject constructor(
    private val userCacheDataStore: UserCacheDataStore,
    private val userRemoteDataStore: UserRemoteDataStore
) {


    /**
     * Return an instance of the Remote Data Store
     */
    open fun retrieveCacheDataStore(): UserDataStore {
        return userCacheDataStore
    }

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveRemoteDataStore(): UserDataStore {
        return userRemoteDataStore
    }

}