package pe.com.bvl.votacionmovil.data.util

interface CommonUtil {

    fun isOnline(): Boolean

    fun logV(psTAG: String, psMessage: String)

    fun logD(psTAG: String, psMessage: String)

    fun logI(psTAG: String, psMessage: String)

    fun logW(psTAG: String, psMessage: String)

    fun logE(psTAG: String, psMessage: String)

    fun logE(psTAG: String, psMessage: String, poException: Throwable)
}