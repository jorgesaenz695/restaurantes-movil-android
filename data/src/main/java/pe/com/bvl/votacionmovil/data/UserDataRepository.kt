package pe.com.bvl.votacionmovil.data

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.data.model.mapper.UserMapper
import pe.com.bvl.votacionmovil.data.source.UserDataStoreFactory
import pe.com.bvl.votacionmovil.domain.repository.UserRepository
import javax.inject.Inject

/**
 * Provides an implementation of the [UserRepository] interface for communicating to and from
 * data sources
 */
class UserDataRepository @Inject constructor(
    private val factory: UserDataStoreFactory,
    private val userMapper: UserMapper
) :

    UserRepository {
    override fun getUser(): Single<User> {
        return factory.retrieveCacheDataStore().getUser().map { userMapper.mapFromEntity(it) }
    }

    override fun login(email: String, password: String): Single<User> {
        val dataStore = factory.retrieveRemoteDataStore()
        var requestLoginEntity = RequestLoginEntity()
        requestLoginEntity.email = email
        requestLoginEntity.password = password

        return dataStore.login(requestLoginEntity)
            .flatMap {
                saveUser(it).toSingle { it }
            }
            .map { userMapper.mapFromEntity(it) }
    }

    private fun saveUser(userEntity: UserEntity): Completable {
        return factory.retrieveCacheDataStore().saveUser(userEntity)
    }
}