package pe.com.bvl.votacionmovil.domain.interactor.vote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.domain.executor.PostExecutionThread
import pe.com.bvl.votacionmovil.domain.executor.ThreadExecutor
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.Vote
import pe.com.bvl.votacionmovil.domain.repository.VoteRepository
import javax.inject.Inject

/**
 * Use case used for retreiving a [List] of [Vote] instances from the [VoteRepository]
 */
open class GetVotesUseCase @Inject constructor(
    val voteRepository: VoteRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    SingleUseCase<List<Vote>, Void?>(threadExecutor, postExecutionThread) {

    public override fun buildUseCaseObservable(params: Void?): Single<List<Vote>> {
        return voteRepository.getVotes()
    }
}