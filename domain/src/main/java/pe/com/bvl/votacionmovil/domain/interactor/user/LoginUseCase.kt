package pe.com.bvl.votacionmovil.domain.interactor.user

import io.reactivex.Single
import pe.com.bvl.votacionmovil.domain.executor.PostExecutionThread
import pe.com.bvl.votacionmovil.domain.executor.ThreadExecutor
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.domain.repository.UserRepository
import javax.inject.Inject

open class LoginUseCase @Inject constructor(
    val userRepository: UserRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    SingleUseCase<User, LoginUseCase.Params>(threadExecutor, postExecutionThread) {


    public override fun buildUseCaseObservable(params: Params?): Single<User> {
        return userRepository.login(params!!.email, params!!.password)
    }


    class Params constructor(var email: String, var password: String) {

    }
}