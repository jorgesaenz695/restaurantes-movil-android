package pe.com.bvl.votacionmovil.domain.model


/**
 * Representation for a [Vote] fetched from an external layer data source
 */
class Vote(
    var title: String? = null,
    var description: String? = null
)