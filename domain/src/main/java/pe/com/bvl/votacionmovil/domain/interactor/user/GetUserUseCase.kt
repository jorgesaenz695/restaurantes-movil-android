package pe.com.bvl.votacionmovil.domain.interactor.user

import io.reactivex.Single
import pe.com.bvl.votacionmovil.domain.executor.PostExecutionThread
import pe.com.bvl.votacionmovil.domain.executor.ThreadExecutor
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.domain.repository.UserRepository
import javax.inject.Inject

open class GetUserUseCase @Inject constructor(
    val userRepository: UserRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    SingleUseCase<User, Void>(threadExecutor, postExecutionThread) {


    override fun buildUseCaseObservable(params: Void?): Single<User> {
        return userRepository.getUser()
    }
}