package pe.com.bvl.votacionmovil.domain.interactor.categories

import io.reactivex.Single
import pe.com.bvl.votacionmovil.domain.executor.PostExecutionThread
import pe.com.bvl.votacionmovil.domain.executor.ThreadExecutor
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.Categories
import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.domain.repository.CategoriesRepository
import pe.com.bvl.votacionmovil.domain.repository.UserRepository
import javax.inject.Inject

open class GetCategoriesUseCase @Inject constructor(
    val categoriesRepository: CategoriesRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    SingleUseCase<List<Categories>, Void>(threadExecutor, postExecutionThread) {


    override fun buildUseCaseObservable(params: Void?): Single<List<Categories>> {
        return categoriesRepository.categories()
    }
}