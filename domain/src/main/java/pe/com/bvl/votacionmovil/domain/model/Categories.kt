package pe.com.bvl.votacionmovil.domain.model

class Categories (
    var id: Int? =null,
    var name: String? = null,
    var description: String? = null
)