package pe.com.bvl.votacionmovil.domain.repository

import pe.com.bvl.votacionmovil.domain.model.User
import io.reactivex.Single

interface UserRepository {

    fun login(email: String, password: String): Single<User>

    fun getUser(): Single<User>

}