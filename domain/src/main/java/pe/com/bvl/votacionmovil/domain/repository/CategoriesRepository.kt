package pe.com.bvl.votacionmovil.domain.repository

import io.reactivex.Single
import pe.com.bvl.votacionmovil.domain.model.Categories

interface CategoriesRepository {

    fun categories(): Single<List<Categories>>

}