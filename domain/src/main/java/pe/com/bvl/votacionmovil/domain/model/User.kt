package pe.com.bvl.votacionmovil.domain.model

/**
 * Representation for a [User] fetched from an external layer data source
 */
data class User(val name: String? = null, val lastName: String? = null, val motherLastName: String? = null)