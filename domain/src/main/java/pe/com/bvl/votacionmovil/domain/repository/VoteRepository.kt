package pe.com.bvl.votacionmovil.domain.repository

import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.domain.model.Vote

/**
 * Interface defining methods for how the data layer can pass data to and from the Domain layer.
 * This is to be implemented by the data layer, setting the requirements for the
 * operations that need to be implemented
 */
interface VoteRepository {

    fun clearVotes(): Completable

    fun saveVotes(votes: List<Vote>): Completable

    fun getVotes(): Single<List<Vote>>

}