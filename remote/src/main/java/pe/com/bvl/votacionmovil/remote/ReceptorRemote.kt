package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import io.reactivex.functions.Function
import pe.com.bvl.votacionmovil.data.exception.BaseException
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import java.io.IOException

open class ReceptorRemote<T> constructor(private val commonUtil: CommonUtil) {

    internal val TAG = ReceptorRemote::class.java.simpleName


    fun invoke(single: Single<ResponseRem<T>>): Single<T> {
        if (commonUtil.isOnline()) {
            return single
                .onErrorResumeNext(Function {
                    if (it is IOException) {
                        var messageError: String = "No es posible conectarse al servidor"
                        commonUtil.logE(TAG, "invoke | onErrorResumeNext| conection", Throwable(messageError))
                        return@Function error(BaseException(BaseException.CONNECTION_SERVER, messageError, it))
                    } else {
                        var messageError: String = "Error parseando la data"
                        commonUtil.logE(TAG, "invoke | onErrorResumeNext | data", Throwable(messageError))
//                        return@Function Single.error(Throwable("Error"))
                        return@Function error(BaseException(BaseException.ERROR_DATA_RESPONSE, messageError, it))
                    }
                })
                .map(Function {
                    if (it.code.equals(BaseException.OK)) {
                        return@Function it.data
                    } else {
                        val messageError: String = it!!.msg ?: it!!.msgError ?: "Error en el servicio."
                        throw BaseException(it.code!!, messageError, it.msgError ?: "")
                    }
                })
        } else {
            var messageError: String = "No tiene conexión a internet"
            commonUtil.logE(TAG, "invoke", Throwable(messageError))
            return error(BaseException(BaseException.ERROR_CONNECTION, messageError))
        }
    }
}