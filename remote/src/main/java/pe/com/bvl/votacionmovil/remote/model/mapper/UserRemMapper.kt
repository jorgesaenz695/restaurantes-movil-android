package pe.com.bvl.votacionmovil.remote.model.mapper

import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.remote.model.UserRem
import javax.inject.Inject

/**
 * Map a [UserRem] to and from a [UserEntity] instance when data is moving between
 * this later and the Data layer
 */
open class UserRemMapper @Inject constructor() : RemMapper<UserRem, UserEntity> {

    /**
     * Map an instance of a [UserRem] to a [UserEntity] model
     */
    override fun mapFromRemote(type: UserRem): UserEntity {
        return UserEntity(type.name, type.lastName, type.motherLastName)
    }

    override fun mapToRemote(type: UserEntity): UserRem {
        val userModel = UserRem()
        userModel.name = type.name
        userModel.lastName = type.lastName
        userModel.motherLastName = type.motherLastName
        return userModel
    }

}