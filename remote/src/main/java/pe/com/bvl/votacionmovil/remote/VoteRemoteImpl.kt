package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.data.repository.VoteRemote
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.remote.model.VoteRem
import pe.com.bvl.votacionmovil.remote.model.mapper.VoteRemMapper
import javax.inject.Inject

/**
 * Remote implementation for retrieving Vote instances. This class implements the
 * [VoteRemote] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class VoteRemoteImpl @Inject constructor(
    private val voteService: VoteService,
    private val voteRemMapper: VoteRemMapper,
    private val commonUtil: CommonUtil
) : VoteRemote {
    override fun getVotes(): Single<List<VoteEntity>> {

        var single = voteService.getVotes()
        var receptorRem: ReceptorRemote<List<VoteRem>> =
            ReceptorRemote(commonUtil)
        return receptorRem.invoke(single)
            .map {
                it.map { listItem ->
                    voteRemMapper.mapFromRemote(listItem)
                }
            }
    }
}