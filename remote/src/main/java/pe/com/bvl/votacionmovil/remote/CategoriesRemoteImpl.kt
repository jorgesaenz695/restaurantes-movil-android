package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.CategoriesEntity
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.remote.model.mapper.RequestLoginRemMapper
import pe.com.bvl.votacionmovil.remote.model.mapper.UserRemMapper
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.data.repository.CategoriesRemote
import pe.com.bvl.votacionmovil.data.repository.UserRemote
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.remote.model.CategoriesRem
import pe.com.bvl.votacionmovil.remote.model.UserRem
import pe.com.bvl.votacionmovil.remote.model.mapper.CategoriesRemMapper
import javax.inject.Inject

/**
 * Remote implementation for retrieving User instances. This class implements the
 * [UserRemote] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class CategoriesRemoteImpl @Inject constructor(
    private val categoriesService: CategoriesService,
    private val categoriesRemMapper: CategoriesRemMapper,
    private val commonUtil: CommonUtil

) : CategoriesRemote {

    override fun categories(): Single<List<CategoriesEntity>> {
        var single = categoriesService.getCategories()
        var receptorRem : ReceptorRemote<List<CategoriesRem>> = ReceptorRemote(commonUtil)

        return receptorRem.invoke(single).map {
                t -> categoriesRemMapper.mapFromRemote(t)
        }
    }

}