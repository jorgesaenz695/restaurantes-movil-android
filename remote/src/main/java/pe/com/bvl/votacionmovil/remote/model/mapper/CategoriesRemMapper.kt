package pe.com.bvl.votacionmovil.remote.model.mapper

import pe.com.bvl.votacionmovil.data.model.CategoriesEntity
import pe.com.bvl.votacionmovil.remote.model.CategoriesRem
import javax.inject.Inject

/**
 * Map a [CategoriesRem] to and from a [CategoriesEntity] instance when data is moving between
 * this later and the Data layer
 */
open class CategoriesRemMapper @Inject constructor() : RemMapper<List<CategoriesRem>, List<CategoriesEntity>> {

    /**
     * Map an instance of a [CategoriesRem] to a [CategoriesEntity] model
     */
    override fun mapFromRemote(type: List<CategoriesRem>): List<CategoriesEntity> {

        val list = ArrayList<CategoriesEntity>()

        for(categories in type){
            list.add(CategoriesEntity(categories.id, categories.name, categories.description))
        }

        return list
    }

    override fun mapToRemote(type: List<CategoriesEntity>): List<CategoriesRem> {

        val list = ArrayList<CategoriesRem>()

        for(categories in type){
            list.add(CategoriesRem(categories.id, categories.name, categories.description))
        }

        return list
    }

}