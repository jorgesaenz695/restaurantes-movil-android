package pe.com.bvl.votacionmovil.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Representation for a [UserRem] fetched from the API
 */
class UserRem(
    @SerializedName("nombre")
    var name: String? = null,
    @SerializedName("apePaterno")
    var lastName: String? = null,
    @SerializedName("apeMaterno")
    var motherLastName: String? = null
)


