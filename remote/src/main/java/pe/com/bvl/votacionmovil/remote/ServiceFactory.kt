package pe.com.bvl.votacionmovil.remote

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Provide "make" methods to create instances of [UserService]
 * and its related dependencies, such as OkHttpClient, Gson, etc.
 */
object ServiceFactory {

    val URL_BASE = "http://demo1298586.mockable.io"
    val URL_USER = "/services/WSUsuario/"
    val URL_VOTE = "/services/WSVotaciones/"

    val URL_CATEGORIES = "/services/WSRestaurant/"

    fun makeUserService(isDebug: Boolean): UserService {
        val okHttpClient = makeOkHttpClient(
            makeLoggingInterceptor(isDebug)
        )
        var baseUrl = "$URL_BASE$URL_USER"
        return makeService(okHttpClient, makeGson(), baseUrl).create(UserService::class.java)
    }

    fun makeVoteService(isDebug: Boolean): VoteService {
        val okHttpClient = makeOkHttpClient(
            makeLoggingInterceptor(isDebug)
        )
        var baseUrl = "$URL_BASE$URL_VOTE"
        return makeService(okHttpClient, makeGson(), baseUrl).create(VoteService::class.java)
    }

    fun makeCategoriesService(isDebug: Boolean): CategoriesService{
        val okHttpClient = makeOkHttpClient(
            makeLoggingInterceptor(isDebug)
        )
        val baseUrl = "$URL_BASE$URL_CATEGORIES"
        return makeService(okHttpClient, makeGson(),baseUrl).create(CategoriesService::class.java)
    }


    private fun makeService(okHttpClient: OkHttpClient, gson: Gson, baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(3, TimeUnit.SECONDS)
            .readTimeout(3, TimeUnit.SECONDS)
            .build()
    }

    private fun makeGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
        return logging
    }

}