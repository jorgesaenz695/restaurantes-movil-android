package pe.com.bvl.votacionmovil.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Representation for a [VoteRem] fetched from the API
 */
class VoteRem(
    @SerializedName("titulo")
    var title: String? = null,
    @SerializedName("descripcion")
    var description: String? = null
)