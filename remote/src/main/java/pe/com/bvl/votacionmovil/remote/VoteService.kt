package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.model.VoteRem
import retrofit2.http.GET

/**
 * Defines the abstract methods used for interacting with USER API
 */
interface VoteService {

    @GET("opVotaciones")
    fun getVotes(): Single<ResponseRem<List<VoteRem>>>

}
