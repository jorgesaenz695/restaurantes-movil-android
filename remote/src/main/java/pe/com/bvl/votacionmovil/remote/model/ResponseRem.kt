package pe.com.bvl.votacionmovil.remote.model

import com.google.gson.annotations.SerializedName

class ResponseRem<T> {

    @SerializedName("data")
    var data: T? = null;
    @SerializedName("codigo")
    var code: String? = null
    @SerializedName("mensaje")
    var msg: String? = null
    @SerializedName("mensajeError")
    var msgError: String? = null

}