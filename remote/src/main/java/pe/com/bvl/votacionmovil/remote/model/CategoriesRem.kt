package pe.com.bvl.votacionmovil.remote.model

import com.google.gson.annotations.SerializedName

class CategoriesRem (
    @SerializedName("id")
    var id: Int? =null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("description")
    var description: String? = null
)