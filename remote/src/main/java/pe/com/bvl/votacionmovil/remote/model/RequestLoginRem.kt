package pe.com.bvl.votacionmovil.remote.model

import com.google.gson.annotations.SerializedName

class RequestLoginRem(
    @SerializedName("codigo")
    var email: String? = null,
    @SerializedName("password")
    var password: String? = null
)