package pe.com.bvl.votacionmovil.remote.model.mapper

import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.remote.model.VoteRem
import javax.inject.Inject

/**
 * Map a [VoteRem] to and from a [VoteEntity] instance when data is moving between
 * this later and the Data layer
 */
open class VoteRemMapper @Inject constructor() : RemMapper<VoteRem, VoteEntity> {

    /**
     * Map an instance of a [VoteRem] to a [VoteEntity] model
     */
    override fun mapFromRemote(type: VoteRem): VoteEntity {
        return VoteEntity(type.title, type.description)
    }

    override fun mapToRemote(type: VoteEntity): VoteRem {
        return VoteRem(type.title, type.description)
    }

}