package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.remote.model.UserRem
import pe.com.bvl.votacionmovil.remote.model.RequestLoginRem
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Defines the abstract methods used for interacting with USER API
 */
interface UserService {

    @POST("opLogin")
    fun login(@Body requestLoginRem: RequestLoginRem): Single<ResponseRem<UserRem>>

}
