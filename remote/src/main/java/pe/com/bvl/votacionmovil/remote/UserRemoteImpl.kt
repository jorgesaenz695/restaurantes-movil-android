package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.remote.model.mapper.RequestLoginRemMapper
import pe.com.bvl.votacionmovil.remote.model.mapper.UserRemMapper
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.data.repository.UserRemote
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.remote.model.UserRem
import javax.inject.Inject

/**
 * Remote implementation for retrieving User instances. This class implements the
 * [UserRemote] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class UserRemoteImpl @Inject constructor(
    private val userService: UserService,
    private val userRemMapper: UserRemMapper,
    private val loginRemMapper: RequestLoginRemMapper,
    private val commonUtil: CommonUtil
) : UserRemote {

    override fun login(requestLoginEntity: RequestLoginEntity): Single<UserEntity> {
        var single = userService.login(loginRemMapper.mapToRemote(requestLoginEntity))
        var receptorRem: ReceptorRemote<UserRem> =
            ReceptorRemote(commonUtil)
        return receptorRem.invoke(single)
            .map { t ->
                userRemMapper.mapFromRemote(t)
            }
    }
}