package pe.com.bvl.votacionmovil.remote.model.mapper

import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.remote.model.RequestLoginRem
import javax.inject.Inject

/**
 * Map a [RequestLoginRem] to and from a [RequestLoginEntity] instance when data is moving between
 * this later and the Data layer
 */
open class RequestLoginRemMapper @Inject constructor() : RemMapper<RequestLoginRem, RequestLoginEntity> {

    /**
     * Map an instance of a [RequestLoginRem] to a [RequestLoginEntity] model
     */
    override fun mapFromRemote(type: RequestLoginRem): RequestLoginEntity {
        val requestLoginEntity = RequestLoginEntity()
        requestLoginEntity.email = type.email
        requestLoginEntity.password = type.password
        return requestLoginEntity
    }

    override fun mapToRemote(type: RequestLoginEntity): RequestLoginRem {
        val requestLoginModel = RequestLoginRem()
        requestLoginModel.email = type.email
        requestLoginModel.password = type.password
        return requestLoginModel
    }

}