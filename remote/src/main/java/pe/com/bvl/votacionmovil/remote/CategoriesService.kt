package pe.com.bvl.votacionmovil.remote

import io.reactivex.Single
import pe.com.bvl.votacionmovil.remote.model.CategoriesRem
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.model.VoteRem
import retrofit2.http.GET

/**
 * Defines the abstract methods used for interacting with CATEGORIES API
 */
interface CategoriesService {

    @GET("opCategories")
    fun getCategories() : Single<ResponseRem<List<CategoriesRem>>>

}
