package pe.com.bvl.votacionmovil.remote

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.model.VoteRem
import pe.com.bvl.votacionmovil.remote.model.mapper.VoteRemMapper
import pe.com.bvl.votacionmovil.remote.test.factory.CommonUtilFactory
import pe.com.bvl.votacionmovil.remote.test.factory.VoteFactory

@RunWith(JUnit4::class)
class VoteRemoteImplTest {

    private lateinit var remMapp: VoteRemMapper
    private lateinit var voteService: VoteService
    private lateinit var commonUtil: CommonUtil

    private lateinit var voteRemoteImpl: VoteRemoteImpl

    @Before
    fun setup() {
        remMapp = mock()
        voteService = mock()
        commonUtil = CommonUtilFactory()

        voteRemoteImpl = VoteRemoteImpl(voteService, remMapp, commonUtil)
    }

    //<editor-fold desc="Get Votes">
    @Test
    fun getVotesCompletes() {
        stubVoteServiceGetVotes(Single.just(VoteFactory.makeVoteResponse()))
        val testObserver = voteRemoteImpl.getVotes().test()
        testObserver.assertComplete()
    }

    @Test
    fun getVotesReturnsData() {
        val voteResponse = VoteFactory.makeVoteResponse()
        stubVoteServiceGetVotes(Single.just(voteResponse))
        val voteEntities = mutableListOf<VoteEntity>()
        voteResponse.data!!.forEach {
            voteEntities.add(remMapp.mapFromRemote(it))
        }

        val testObserver = voteRemoteImpl.getVotes().test()
        testObserver.assertValue(voteEntities)
    }
    //</editor-fold>

    private fun stubVoteServiceGetVotes(single: Single<ResponseRem<List<VoteRem>>>) {
        whenever(voteService.getVotes())
            .thenReturn(single)
    }
}