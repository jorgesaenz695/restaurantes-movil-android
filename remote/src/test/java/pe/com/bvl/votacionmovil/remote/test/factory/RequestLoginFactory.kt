package pe.com.bvl.votacionmovil.remote.test.factory

import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.remote.model.RequestLoginRem
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for RequestLogin related instances
 */
class RequestLoginFactory {

    companion object Factory {

        fun makeRequestLoginResponse(): ResponseRem<List<RequestLoginRem>> {
            val requestLoginResponse = ResponseRem<List<RequestLoginRem>>()
            requestLoginResponse.data = makeRequestLoginRemList(5)
            return requestLoginResponse
        }

        fun makeRequestLoginRemList(count: Int): List<RequestLoginRem> {
            val requestLoginEntities = mutableListOf<RequestLoginRem>()
            repeat(count) {
                requestLoginEntities.add(makeRequestLoginRem())
            }
            return requestLoginEntities
        }

        fun makeRequestLoginRem(): RequestLoginRem {
            return RequestLoginRem(randomUuid(), randomUuid())
        }


        fun makeRequestLoginEntity(): RequestLoginEntity {
            return RequestLoginEntity(randomUuid(), randomUuid())
        }

    }

}