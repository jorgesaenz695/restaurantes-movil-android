package pe.com.bvl.votacionmovil.remote.test.factory

import pe.com.bvl.votacionmovil.data.exception.BaseException
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.remote.VoteService
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.model.VoteRem
import pe.com.bvl.votacionmovil.remote.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for Vote related instances
 */
class VoteFactory {

    companion object Factory {

        fun makeVoteResponse(): ResponseRem<List<VoteRem>> {
            val voteResponse = ResponseRem<List<VoteRem>>()
            voteResponse.code = BaseException.OK
            voteResponse.data = makeVoteRemList(5)
            return voteResponse
        }

        fun makeVoteRemList(count: Int): List<VoteRem> {
            val voteEntities = mutableListOf<VoteRem>()
            repeat(count) {
                voteEntities.add(makeVoteRem())
            }
            return voteEntities
        }

        fun makeVoteRem(): VoteRem {
            return VoteRem(randomUuid(), randomUuid())
        }


        fun makeVoteEntity(): VoteEntity {
            return VoteEntity(randomUuid(), randomUuid())
        }

    }

}