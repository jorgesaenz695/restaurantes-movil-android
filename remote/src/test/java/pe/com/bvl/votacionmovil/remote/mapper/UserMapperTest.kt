package pe.com.bvl.votacionmovil.remote.mapper

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.remote.model.mapper.UserRemMapper
import pe.com.bvl.votacionmovil.remote.test.factory.UserFactory
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class UserMapperTest {

    private lateinit var userRemMapper: UserRemMapper

    @Before
    fun setUp() {
        userRemMapper = UserRemMapper()
    }

    @Test
    fun mapFromRemoteMapsData() {
        val userRem = UserFactory.makeUserRem()
        val userEntity = userRemMapper.mapFromRemote(userRem)

        assertEquals(userRem.name, userEntity.name)
        assertEquals(userRem.lastName, userEntity.lastName)
        assertEquals(userRem.motherLastName, userEntity.motherLastName)
    }

    @Test
    fun mapToRemoteMapsData() {
        val userEntity = UserFactory.makeUserEntity()
        val userRem = userRemMapper.mapToRemote(userEntity)

        assertEquals(userRem.name, userEntity.name)
        assertEquals(userRem.lastName, userEntity.lastName)
        assertEquals(userRem.motherLastName, userEntity.motherLastName)
    }

}