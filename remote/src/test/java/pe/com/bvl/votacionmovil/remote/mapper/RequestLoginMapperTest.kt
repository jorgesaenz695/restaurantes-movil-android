package pe.com.bvl.votacionmovil.remote.mapper

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.remote.model.mapper.RequestLoginRemMapper
import pe.com.bvl.votacionmovil.remote.test.factory.RequestLoginFactory
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class RequestLoginMapperTest {

    private lateinit var requestLoginRemMapper: RequestLoginRemMapper

    @Before
    fun setUp() {
        requestLoginRemMapper = RequestLoginRemMapper()
    }

    @Test
    fun mapFromRemoteMapsData() {
        val requestLoginRem = RequestLoginFactory.makeRequestLoginRem()
        val requestLoginEntity = requestLoginRemMapper.mapFromRemote(requestLoginRem)

        assertEquals(requestLoginRem.email, requestLoginEntity.email)
        assertEquals(requestLoginRem.password, requestLoginEntity.password)
    }

    @Test
    fun mapToRemoteMapsData() {
        val requestLoginEntity = RequestLoginFactory.makeRequestLoginEntity()
        val requestLoginRem = requestLoginRemMapper.mapToRemote(requestLoginEntity)

        assertEquals(requestLoginRem.email, requestLoginEntity.email)
        assertEquals(requestLoginRem.password, requestLoginEntity.password)
    }

}