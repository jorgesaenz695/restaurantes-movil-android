package pe.com.bvl.votacionmovil.remote.test.factory

import pe.com.bvl.votacionmovil.data.exception.BaseException
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.model.UserRem
import pe.com.bvl.votacionmovil.remote.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for User related instances
 */
class UserFactory {

    companion object Factory {

        fun makeLoginResponse(): ResponseRem<UserRem> {
            val loginResponse = ResponseRem<UserRem>()
            loginResponse.code = BaseException.OK
            loginResponse.data = makeUserRem()
            return loginResponse
        }

        fun makeUserRemList(count: Int): List<UserRem> {
            val userEntities = mutableListOf<UserRem>()
            repeat(count) {
                userEntities.add(makeUserRem())
            }
            return userEntities
        }

        fun makeUserRem(): UserRem {
            return UserRem(randomUuid(), randomUuid(), randomUuid())
        }


        fun makeUserEntity(): UserEntity {
            return UserEntity(randomUuid(), randomUuid(), randomUuid())
        }

    }

}