package pe.com.bvl.votacionmovil.remote.mapper

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.remote.model.mapper.VoteRemMapper
import pe.com.bvl.votacionmovil.remote.test.factory.VoteFactory
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class VoteMapperTest {

    private lateinit var voteRemMapper: VoteRemMapper

    @Before
    fun setUp() {
        voteRemMapper = VoteRemMapper()
    }

    @Test
    fun mapFromRemoteMapsData() {
        val voteRem = VoteFactory.makeVoteRem()
        val voteEntity = voteRemMapper.mapFromRemote(voteRem)

        assertEquals(voteRem.title, voteEntity.title)
        assertEquals(voteRem.description, voteEntity.description)
    }

    @Test
    fun mapToRemoteMapsData() {
        val voteEntity = VoteFactory.makeVoteEntity()
        val voteRem = voteRemMapper.mapToRemote(voteEntity)

        assertEquals(voteRem.title, voteEntity.title)
        assertEquals(voteRem.description, voteEntity.description)
    }

}