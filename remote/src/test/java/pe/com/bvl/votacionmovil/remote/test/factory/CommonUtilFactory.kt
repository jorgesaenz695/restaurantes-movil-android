package pe.com.bvl.votacionmovil.remote.test.factory

import pe.com.bvl.votacionmovil.data.util.CommonUtil

class CommonUtilFactory : CommonUtil {
    override fun isOnline(): Boolean {
        return true
    }

    override fun logV(psTAG: String, psMessage: String) {
    }

    override fun logD(psTAG: String, psMessage: String) {
    }

    override fun logI(psTAG: String, psMessage: String) {
    }

    override fun logW(psTAG: String, psMessage: String) {
    }

    override fun logE(psTAG: String, psMessage: String) {
    }

    override fun logE(psTAG: String, psMessage: String, poException: Throwable) {
    }

}