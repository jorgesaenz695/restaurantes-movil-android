package pe.com.bvl.votacionmovil.remote

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.data.model.RequestLoginEntity
import pe.com.bvl.votacionmovil.data.util.CommonUtil
import pe.com.bvl.votacionmovil.remote.model.ResponseRem
import pe.com.bvl.votacionmovil.remote.model.UserRem
import pe.com.bvl.votacionmovil.remote.model.mapper.RequestLoginRemMapper
import pe.com.bvl.votacionmovil.remote.model.mapper.UserRemMapper
import pe.com.bvl.votacionmovil.remote.test.factory.CommonUtilFactory
import pe.com.bvl.votacionmovil.remote.test.factory.RequestLoginFactory
import pe.com.bvl.votacionmovil.remote.test.factory.UserFactory

@RunWith(JUnit4::class)
class UserRemoteImplTest {

    private lateinit var remMapp: UserRemMapper
    private lateinit var requestLoginRemMapper: RequestLoginRemMapper
    private lateinit var userService: UserService
    private lateinit var commonUtil: CommonUtil

    private lateinit var userRemoteImpl: UserRemoteImpl

    @Before
    fun setup() {
        remMapp = mock()
        userService = mock()
        requestLoginRemMapper = RequestLoginRemMapper()
        commonUtil = CommonUtilFactory()

        userRemoteImpl = UserRemoteImpl(userService, remMapp, requestLoginRemMapper, commonUtil)
    }

    //<editor-fold desc="Get Users">
    @Test
    fun loginComplete() {

        val requestLoginEntity = RequestLoginFactory.makeRequestLoginEntity()
        stubUserServiceLogin(Single.just(UserFactory.makeLoginResponse()), requestLoginEntity)
        val testObserver = userRemoteImpl.login(requestLoginEntity).test()
        testObserver.assertComplete()
    }

    @Test
    fun loginReturnsData() {
        val userResponse = UserFactory.makeLoginResponse()
        val requestLoginEntity = RequestLoginFactory.makeRequestLoginEntity();
        stubUserServiceLogin(Single.just(userResponse), requestLoginEntity)
        val userEntity = remMapp.mapFromRemote(userResponse.data!!)

        val testObserver = userRemoteImpl.login(requestLoginEntity).test()
        testObserver.assertValue(userEntity)
    }
    //</editor-fold>

    private fun stubUserServiceLogin(single: Single<ResponseRem<UserRem>>, requestLoginEntity: RequestLoginEntity) {
        val requestLoginRem = requestLoginRemMapper.mapToRemote(requestLoginEntity)
        whenever(userService.login(requestLoginRem))
            .thenReturn(single)
    }
}