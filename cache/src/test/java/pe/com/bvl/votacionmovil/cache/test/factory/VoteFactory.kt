package pe.com.bvl.votacionmovil.cache.test.factory

import pe.com.bvl.votacionmovil.cache.dto.VoteDto
import pe.com.bvl.votacionmovil.cache.test.factory.DataFactory.Factory.randomUuid
import pe.com.bvl.votacionmovil.data.model.VoteEntity

/**
 * Factory class for Vote related instances
 */
class VoteFactory {

    companion object Factory {

        fun makeVoteCache(): VoteDto {
            return VoteDto(title = randomUuid(), description = randomUuid())
        }

        fun makeVoteEntity(): VoteEntity {
            return VoteEntity(randomUuid(), randomUuid())
        }

        fun makeVoteEntityList(count: Int): List<VoteEntity> {
            val voteEntities = mutableListOf<VoteEntity>()
            repeat(count) {
                voteEntities.add(makeVoteEntity())
            }
            return voteEntities
        }

        fun makeVoteCacheList(count: Int): List<VoteDto> {
            val cachedVotes = mutableListOf<VoteDto>()
            repeat(count) {
                cachedVotes.add(makeVoteCache())
            }
            return cachedVotes
        }

    }

}