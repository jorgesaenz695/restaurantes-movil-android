package pe.com.bvl.votacionmovil.cache.dto.mapper

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.cache.dto.UserDto
import pe.com.bvl.votacionmovil.cache.test.factory.UserFactory
import pe.com.bvl.votacionmovil.data.model.UserEntity
import kotlin.test.assertEquals


@RunWith(JUnit4::class)
class UserDtoMapperTest {

    private lateinit var userEntityMapper: UserCacheMapper

    @Before
    fun setUp() {
        userEntityMapper = UserCacheMapper()
    }

    @Test
    fun mapToCachedMapsData() {
        val userDto = UserFactory.makeUserCache()
        val userEntity = userEntityMapper.mapFromCached(userDto)

        assertUserDataEquality(userEntity, userDto)
    }

    @Test
    fun mapFromCachedMapsData() {
        val cachedUser = UserFactory.makeUserCache()
        val userEntity = userEntityMapper.mapFromCached(cachedUser)

        assertUserDataEquality(userEntity, cachedUser)
    }

    private fun assertUserDataEquality(
        userEntity: UserEntity,
        userDto: UserDto
    ) {
        assertEquals(userEntity.name, userDto.name)
        assertEquals(userEntity.lastName, userDto.lastName)
        assertEquals(userEntity.motherLastName, userDto.motherLastName)
    }

}