package pe.com.bvl.votacionmovil.cache.test.factory

import pe.com.bvl.votacionmovil.cache.dto.UserDto
import pe.com.bvl.votacionmovil.cache.test.factory.DataFactory.Factory.randomUuid
import pe.com.bvl.votacionmovil.data.model.UserEntity

/**
 * Factory class for User related instances
 */
class UserFactory {

    companion object Factory {

        fun makeUserCache(): UserDto {
            return UserDto(name = randomUuid(), lastName = randomUuid(), motherLastName = randomUuid())
        }

        fun makeUserEntity(): UserEntity {
            return UserEntity(randomUuid(), randomUuid(), randomUuid())
        }

        fun makeUserEntityList(count: Int): List<UserEntity> {
            val bufferooEntities = mutableListOf<UserEntity>()
            repeat(count) {
                bufferooEntities.add(makeUserEntity())
            }
            return bufferooEntities
        }

        fun makeUserCacheList(count: Int): List<UserDto> {
            val cachedUsers = mutableListOf<UserDto>()
            repeat(count) {
                cachedUsers.add(makeUserCache())
            }
            return cachedUsers
        }

    }

}