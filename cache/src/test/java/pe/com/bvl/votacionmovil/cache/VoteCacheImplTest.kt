package pe.com.bvl.votacionmovil.cache

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import pe.com.bvl.votacionmovil.cache.db.ObjectBox
import pe.com.bvl.votacionmovil.cache.dto.VoteDto
import pe.com.bvl.votacionmovil.cache.dto.mapper.VoteCacheMapper
import pe.com.bvl.votacionmovil.cache.test.factory.VoteFactory
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(21))
class VoteCacheImplTest {

    private var cacheMapper = VoteCacheMapper()
    private var preferencesHelper = PreferencesHelper(RuntimeEnvironment.systemContext)

    private val voteChacheImpl = VoteCacheImpl(
        cacheMapper,
        preferencesHelper
    )

    @Before
    fun setup() {
        ObjectBox.build(RuntimeEnvironment.systemContext)
    }

    @Test
    fun clearTablesCompletes() {
        val testObserver = voteChacheImpl.clearVotes().test()
        testObserver.assertComplete()
    }

    //<editor-fold desc="Save Votes">
    @Test
    fun saveVotesCompletes() {
        val bufferooEntities = VoteFactory.makeVoteEntityList(2)

        val testObserver = voteChacheImpl.saveVotes(bufferooEntities).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveVotesSavesData() {
        val bufferooCount = 2
        val bufferooEntities = VoteFactory.makeVoteEntityList(bufferooCount)

        voteChacheImpl.saveVotes(bufferooEntities).test()
        checkNumRowsInVotesTable(bufferooCount.toLong())
    }
    //</editor-fold>

    //<editor-fold desc="Get Votes">
    @Test
    fun getVotesCompletes() {
        val testObserver = voteChacheImpl.getVotes().test()
        testObserver.assertComplete()
    }

    @Test
    fun getVotesReturnsData() {
        val voteEntities = VoteFactory.makeVoteEntityList(2)
        val voteDtos = mutableListOf<VoteDto>()
        voteEntities.forEach {
            voteDtos.add(cacheMapper.mapToCached(it))
        }
        insertVotes(voteDtos)

        val testObserver = voteChacheImpl.getVotes().test()
        testObserver.assertValue(voteEntities)
    }
    //</editor-fold>

    private fun insertVotes(voteDtos: List<VoteDto>) {

        voteChacheImpl.getBox().put(voteDtos)
    }

    private fun checkNumRowsInVotesTable(expectedRows: Long) {
        var numberOfRows = voteChacheImpl.getBox().count()
        assertEquals(expectedRows, numberOfRows)
    }

}