package pe.com.bvl.votacionmovil.cache.dto.mapper

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pe.com.bvl.votacionmovil.cache.dto.VoteDto
import pe.com.bvl.votacionmovil.cache.test.factory.VoteFactory
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import kotlin.test.assertEquals


@RunWith(JUnit4::class)
class VoteDtoMapperTest {

    private lateinit var voteEntityMapper: VoteCacheMapper

    @Before
    fun setUp() {
        voteEntityMapper = VoteCacheMapper()
    }

    @Test
    fun mapToCachedMapsData() {
        val voteDto = VoteFactory.makeVoteCache()
        val voteEntity = voteEntityMapper.mapFromCached(voteDto)

        assertVoteDataEquality(voteEntity, voteDto)
    }

    @Test
    fun mapFromCachedMapsData() {
        val cachedVote = VoteFactory.makeVoteCache()
        val voteEntity = voteEntityMapper.mapFromCached(cachedVote)

        assertVoteDataEquality(voteEntity, cachedVote)
    }

    private fun assertVoteDataEquality(
        voteEntity: VoteEntity,
        voteDto: VoteDto
    ) {
        assertEquals(voteEntity.title, voteDto.title)
        assertEquals(voteEntity.description, voteDto.description)
    }

}