package pe.com.bvl.votacionmovil.cache

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import pe.com.bvl.votacionmovil.cache.db.ObjectBox
import pe.com.bvl.votacionmovil.cache.dto.UserDto
import pe.com.bvl.votacionmovil.cache.dto.mapper.UserCacheMapper
import pe.com.bvl.votacionmovil.cache.test.factory.UserFactory
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(21))
class UserCacheImplTest {

    private var cacheMapper = UserCacheMapper()

    private val userChacheImpl = UserCacheImpl(
        cacheMapper
    )

    @Before
    fun setup() {
        ObjectBox.build(RuntimeEnvironment.systemContext)
    }

    @Test
    fun clearTablesCompletes() {
        val testObserver = userChacheImpl.clearUser().test()
        testObserver.assertComplete()
    }

    //<editor-fold desc="Save User">
    @Test
    fun saveUserCompletes() {
        val userEntity = UserFactory.makeUserEntity()
        val testObserver = userChacheImpl.saveUser(userEntity).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveUserSavesData() {
        val userEntity = UserFactory.makeUserEntity()



        userChacheImpl.saveUser(userEntity).test()
        checkNumRowsInUserTable(1)
    }
    //</editor-fold>

    //<editor-fold desc="Get User">
    @Test
    fun getUserCompletes() {
        val testObserver = userChacheImpl.getUser().test()
        testObserver.assertComplete()
    }


    private fun insertUser(userDtos: List<UserDto>) {

        userChacheImpl.getBox().put(userDtos)
    }

    private fun checkNumRowsInUserTable(expectedRows: Long) {
        var numberOfRows = userChacheImpl.getBox().count()
        assertEquals(expectedRows, numberOfRows)
    }

}