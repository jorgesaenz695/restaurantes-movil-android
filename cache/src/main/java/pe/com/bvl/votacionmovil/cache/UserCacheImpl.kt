package pe.com.bvl.votacionmovil.cache

import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.cache.db.ObjectBox
import pe.com.bvl.votacionmovil.cache.dto.UserDto
import pe.com.bvl.votacionmovil.cache.dto.mapper.UserCacheMapper
import pe.com.bvl.votacionmovil.data.exception.BaseException
import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.data.repository.UserCache
import javax.inject.Inject

/**
 * Cached implementation for retrieving and saving Bufferoo instances. This class implements the
 * [BufferooCache] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class UserCacheImpl @Inject constructor(
    private val cacheMapper: UserCacheMapper
) :
    UserCache {

    internal fun getBox(): Box<UserDto> {
        return ObjectBox.boxStore.boxFor()
    }


    override fun clearUser(): Completable {
        return Completable.defer {
            getBox().removeAll()
            Completable.complete()
        }
    }

    override fun getUser(): Single<UserEntity> {
        return Single.defer<UserEntity> {
            var userDto = getBox().query().build().findFirst()
            if (userDto != null) {
                Single.just(cacheMapper.mapFromCached(userDto))
            } else {
                Single.error(BaseException(BaseException.ERROR_BD_USER_NOT_EXIST, "Usuario no existe"))
            }
        }
    }

    override fun saveUser(userEntity: UserEntity): Completable {
        return clearUser()
            .doOnComplete {
                getBox().put(cacheMapper.mapToCached(userEntity))
            }
    }


}