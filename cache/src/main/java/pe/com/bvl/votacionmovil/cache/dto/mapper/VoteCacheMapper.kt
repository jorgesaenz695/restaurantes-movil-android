package pe.com.bvl.votacionmovil.cache.dto.mapper

import pe.com.bvl.votacionmovil.cache.dto.VoteDto
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import javax.inject.Inject

/**
 * Map a [CachedVote] instance to and from a [VoteEntity] instance when data is moving between
 * this later and the Data layer
 */
class VoteCacheMapper @Inject constructor() : EntityMapper<VoteDto, VoteEntity> {

    /**
     * Map a [VoteEntity] instance to a [CachedVote] instance
     */
    override fun mapToCached(type: VoteEntity): VoteDto {
        return VoteDto(title = type.title, description = type.description)
    }

    /**
     * Map a [CachedVote] instance to a [VoteEntity] instance
     */
    override fun mapFromCached(type: VoteDto): VoteEntity {
        return VoteEntity(type.title, type.description)
    }

}