package pe.com.bvl.votacionmovil.cache.dto

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.util.Date

@Entity
data class UserDto(
    @Id var id: Long = 0,
    var name: String? = null,
    var lastName: String? = null,
    var motherLastName: String? = null
)
