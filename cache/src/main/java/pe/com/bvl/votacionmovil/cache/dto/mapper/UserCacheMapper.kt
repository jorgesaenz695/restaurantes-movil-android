package pe.com.bvl.votacionmovil.cache.dto.mapper

import pe.com.bvl.votacionmovil.data.model.UserEntity
import pe.com.bvl.votacionmovil.cache.dto.UserDto
import javax.inject.Inject

/**
 * Map a [CachedUser] instance to and from a [UserEntity] instance when data is moving between
 * this later and the Data layer
 */
class UserCacheMapper @Inject constructor() : EntityMapper<UserDto, UserEntity> {

    /**
     * Map a [UserEntity] instance to a [CachedUser] instance
     */
    override fun mapToCached(type: UserEntity): UserDto {
        return UserDto(name = type.name, lastName = type.lastName, motherLastName = type.motherLastName)
    }

    /**
     * Map a [CachedUser] instance to a [UserEntity] instance
     */
    override fun mapFromCached(type: UserDto): UserEntity {
        return UserEntity(type.name, type.lastName, type.motherLastName)
    }

}