package pe.com.bvl.votacionmovil.cache.app

import android.app.Application
import pe.com.bvl.votacionmovil.cache.db.ObjectBox

open class CacheApplication : Application() {


    override fun onCreate() {
        super.onCreate()

        ObjectBox.build(this)
    }


}
