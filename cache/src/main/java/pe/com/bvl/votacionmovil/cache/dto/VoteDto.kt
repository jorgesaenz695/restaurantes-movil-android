package pe.com.bvl.votacionmovil.cache.dto

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.util.Date

@Entity
data class VoteDto(
    @Id var id: Long = 0,
    var title: String? = null,
    var description: String? = null
)
