package pe.com.bvl.votacionmovil.cache

import android.database.sqlite.SQLiteDatabase
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.reactivex.Completable
import io.reactivex.Single
import pe.com.bvl.votacionmovil.cache.db.ObjectBox
import pe.com.bvl.votacionmovil.cache.dto.VoteDto
import pe.com.bvl.votacionmovil.cache.dto.VoteDto_
import pe.com.bvl.votacionmovil.cache.dto.mapper.VoteCacheMapper
import pe.com.bvl.votacionmovil.data.model.VoteEntity
import pe.com.bvl.votacionmovil.data.repository.VoteCache
import javax.inject.Inject

/**
 * Cached implementation for retrieving and saving Bufferoo instances. This class implements the
 * [BufferooCache] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class VoteCacheImpl @Inject constructor(
    private val entityMapper: VoteCacheMapper,
    private val preferencesHelper: PreferencesHelper
) :
    VoteCache {


    private val EXPIRATION_TIME = 10 * 60 * 1000

    internal fun getBox(): Box<VoteDto> {
        return ObjectBox.boxStore.boxFor()
    }


    override fun clearVotes(): Completable {
        return Completable.defer {

            getBox().removeAll()
            Completable.complete()
        }
    }

    override fun saveVotes(votes: List<VoteEntity>): Completable {
        return Completable.defer {
            getBox().put(votes.map { entityMapper.mapToCached(it) })
            Completable.complete()
        }
    }

    override fun getVotes(): Single<List<VoteEntity>> {
        return Single.defer<List<VoteEntity>> {
            val votesQuery = getBox().query {
                order(VoteDto_.title)
            }
            Single.just<List<VoteEntity>>(votesQuery.find().map {
                entityMapper.mapFromCached(it)
            })

        }
    }

    override fun isCached(): Boolean {
        return getBox().count() > 0
    }

    override fun setLastCacheTime(lastCache: Long) {
        preferencesHelper.voteLastCacheTime = lastCache
    }

    override fun isExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val lastUpdateTime = this.getLastCacheUpdateTimeMillis()
        return currentTime - lastUpdateTime > EXPIRATION_TIME
    }

    private fun getLastCacheUpdateTimeMillis(): Long {
        return preferencesHelper.voteLastCacheTime
    }


}