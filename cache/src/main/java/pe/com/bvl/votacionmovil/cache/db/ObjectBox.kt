package pe.com.bvl.votacionmovil.cache.db

import android.content.Context
import io.objectbox.BoxStore
import pe.com.bvl.votacionmovil.cache.dto.MyObjectBox

/**
 * Singleton to keep BoxStore reference.
 */
object ObjectBox {

    lateinit var boxStore: BoxStore
        private set

    fun build(context: Context) {
        // This is the minimal setup required on Android
        boxStore = MyObjectBox.builder().androidContext(context).build()

//        boxStore = MyObjectBox.builder().androidContext(this).build()
//        if (BuildConfig.DEBUG) {
//            val started = AndroidObjectBrowser(boxStore).start(context)
//            Log.i("ObjectBrowser", "Started: $started")
//        }
        // Example how you could use a custom dir in "external storage"
        // (Android 6+ note: give the app storage permission in app info settings)
//        val directory = File(Environment.getExternalStorageDirectory(), "objectbox-notes");
//        boxStore = MyObjectBox.builder().androidContext(context.applicationContext)
//                .directory(directory)
//                .build()
    }

}