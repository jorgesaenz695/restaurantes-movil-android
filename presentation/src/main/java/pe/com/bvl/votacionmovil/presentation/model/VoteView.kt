package pe.com.bvl.votacionmovil.presentation.model

/**
 * Representation for a [VoteView] instance for this layers Model representation
 */
class VoteView(val title: String? = null, val description: String? = null)