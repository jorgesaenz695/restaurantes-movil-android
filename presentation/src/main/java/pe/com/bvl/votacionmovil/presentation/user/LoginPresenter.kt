package pe.com.bvl.votacionmovil.presentation.user

import io.reactivex.observers.DisposableSingleObserver
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.presentation.model.mapper.UserViewMapper
import pe.com.bvl.votacionmovil.domain.interactor.user.LoginUseCase
import javax.inject.Inject

class LoginPresenter @Inject constructor(
    val loginView: LoginContract.View,
    val loginUseCase: SingleUseCase<User, LoginUseCase.Params>,
    val userMapper: UserViewMapper
) :
    LoginContract.Presenter {


    init {
        loginView.setPresenter(this)
    }

    override fun start() {
        // initialize()
    }

    override fun onDestroy() {
        loginView.hideError()
        loginView.showProgress()
        loginUseCase.dispose()
    }

    override fun initialize(email: String, password: String) {
        loginView.showProgress()

        loginUseCase.execute(LoginSubscriber(), LoginUseCase.Params(email, password))
    }

    internal fun handleLoginSuccess(user: User) {
        loginView.hideProgress()
        loginView.showLoginSuccess(userMapper.mapToView(user))

    }

    inner class LoginSubscriber : DisposableSingleObserver<User>() {

        override fun onSuccess(t: User) {
            handleLoginSuccess(t)
        }

        override fun onError(exception: Throwable) {
            loginView.hideProgress()
            loginView.showError(exception)
        }
    }

}