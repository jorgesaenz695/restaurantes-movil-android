package pe.com.bvl.votacionmovil.presentation.model.mapper

import pe.com.bvl.votacionmovil.domain.model.Vote
import pe.com.bvl.votacionmovil.presentation.model.VoteView
import javax.inject.Inject

/**
 * Map a [VoteView] to and from a [Vote] instance when data is moving between
 * this layer and the Domain layer
 */
open class VoteViewMapper @Inject constructor() : ViewMapper<VoteView, Vote> {

    /**
     * Map a [VoteView] instance to a [Vote] instance
     */
    override fun mapFromView(type: VoteView): Vote {
        return Vote(type.title, type.description)
    }

    /**
     * Map a [Vote] instance to a [VoteView] instance
     */
    override fun mapToView(type: Vote): VoteView {
        return VoteView(type.title, type.description)
    }


}