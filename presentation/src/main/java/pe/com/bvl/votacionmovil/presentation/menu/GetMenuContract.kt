package pe.com.bvl.votacionmovil.presentation.vote

/**
 * Defines a contract of operations between the Browse Presenter and Browse View
 */
interface GetMenuContract {

    interface View {

        fun showProgress()

        fun hideProgress()

        fun showError(error: Throwable)

        fun hideError()
    }

}