package pe.com.bvl.votacionmovil.presentation

/**
 * Interface class to act as a base for any class that is to take the role of the IPresenter in the
 * Model-BaseView-IPresenter pattern.
 */
interface BasePresenter {

    fun start()

    fun onDestroy()

}