package pe.com.bvl.votacionmovil.presentation.vote

import pe.com.bvl.votacionmovil.presentation.BasePresenter
import pe.com.bvl.votacionmovil.presentation.model.VoteView

/**
 * Defines a contract of operations between the Browse Presenter and Browse View
 */
interface GetVotesContract {

    interface View {

        fun showProgress()

        fun hideProgress()

        fun showError(error: Throwable)

        fun hideError()

        fun showGetVotesSuccess(votes: List<VoteView>)

        fun setPresenter(presenter: Presenter)


    }

    interface Presenter : BasePresenter {

        fun initialize()

    }

}