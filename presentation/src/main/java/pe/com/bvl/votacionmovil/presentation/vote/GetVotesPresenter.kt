package pe.com.bvl.votacionmovil.presentation.vote

import io.reactivex.observers.DisposableSingleObserver
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.Vote
import pe.com.bvl.votacionmovil.presentation.model.mapper.VoteViewMapper
import javax.inject.Inject

class GetVotesPresenter @Inject constructor(
    val getVotesView: GetVotesContract.View,
    val getVotesUseCase: SingleUseCase<List<Vote>, Void>,
    val voteMapper: VoteViewMapper
) :
    GetVotesContract.Presenter {


    init {
        getVotesView.setPresenter(this)
    }

    override fun start() {
        initialize()
    }

    override fun onDestroy() {
        getVotesView.hideError()
        getVotesView.showProgress()
        getVotesUseCase.dispose()
    }

    override fun initialize() {
        getVotesView.showProgress()

        getVotesUseCase.execute(GetVotesSubscriber())
    }

    internal fun handleGetVotesSuccess(votes: List<Vote>) {
        getVotesView.hideProgress()
        getVotesView.showGetVotesSuccess(votes.map { voteMapper.mapToView(it) })
    }

    inner class GetVotesSubscriber : DisposableSingleObserver<List<Vote>>() {

        override fun onSuccess(t: List<Vote>) {
            handleGetVotesSuccess(t)
        }

        override fun onError(exception: Throwable) {
            getVotesView.hideProgress()
            getVotesView.showError(exception)
        }
    }

}