package pe.com.bvl.votacionmovil.presentation.model

/**
 * Representation for a [CategoriesView] fetched from an external layer data source
 */
data class CategoriesView(var id: Int? =null,
                          var name: String? = null,
                          var description: String? = null)