package pe.com.bvl.votacionmovil.presentation.categories

import io.reactivex.observers.DisposableSingleObserver
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.Categories
import pe.com.bvl.votacionmovil.presentation.model.mapper.CategoriesViewMapper
import javax.inject.Inject

class GetCategoriesPresenter @Inject constructor(
    val getCategoriesView: GetCategoriesContract.View,
    val getCategoriesUseCase: SingleUseCase<List<Categories>, Void>,
    val categoriesViewMapper: CategoriesViewMapper
) :
    GetCategoriesContract.Presenter {

    init {
        getCategoriesView.setPresenter(this)
    }

    override fun start() {
        // initialize()
    }

    override fun onDestroy() {
        getCategoriesView.hideError()
        getCategoriesView.showProgress()
        getCategoriesUseCase.dispose()
    }

    override fun initialize() {
        getCategoriesView.showProgress()

        getCategoriesUseCase.execute(GetCategoriesSubscriber())
    }

    internal fun handleGetUserSuccess(categories: List<Categories>) {
        getCategoriesView.hideProgress()
        getCategoriesView.showGetCategoriesSuccess(categoriesViewMapper.mapToView(categories))

    }

    inner class GetCategoriesSubscriber : DisposableSingleObserver<List<Categories>>() {

        override fun onSuccess(t: List<Categories>) {
            handleGetUserSuccess(t)
        }

        override fun onError(exception: Throwable) {
            getCategoriesView.hideProgress()
            getCategoriesView.showError(exception)
        }
    }

}