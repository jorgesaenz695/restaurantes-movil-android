package pe.com.bvl.votacionmovil.presentation.model.mapper

import pe.com.bvl.votacionmovil.domain.model.Categories
import pe.com.bvl.votacionmovil.presentation.model.CategoriesView
import javax.inject.Inject

/**
 * Map a [CategoriesView] to and from a [Categories] instance when data is moving between
 * this layer and the Domain layer
 */
open class CategoriesViewMapper @Inject constructor() : ViewMapper<List<CategoriesView>, List<Categories>> {

    /**
     * Map a [Categories] instance to a [CategoriesView] instance
     */
    override fun mapToView(type: List<Categories>): List<CategoriesView> {
        var listView = ArrayList<CategoriesView>()

        for(element in type){
            listView.add(CategoriesView(element.id,element.name, element.description))
        }

        return listView
    }

    /**
     * Map a [CategoriesView] instance to a [Categories] instance
     */

    override fun mapFromView(type: List<CategoriesView>): List<Categories> {
        var listView = ArrayList<Categories>()

        for(element in type){
            listView.add(Categories(element.id,element.name, element.description))
        }

        return listView
    }

}