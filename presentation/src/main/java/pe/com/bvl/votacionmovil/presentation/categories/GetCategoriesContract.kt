package pe.com.bvl.votacionmovil.presentation.categories

import pe.com.bvl.votacionmovil.presentation.BasePresenter
import pe.com.bvl.votacionmovil.presentation.model.CategoriesView

interface GetCategoriesContract {
    interface View {

        fun showProgress()

        fun hideProgress()

        fun showGetCategoriesSuccess(categoriesView: List<CategoriesView>)

        fun showError(error: Throwable)

        fun hideError()


        fun setPresenter(presenter: Presenter)
    }

    interface Presenter : BasePresenter {
        fun initialize()
    }
}