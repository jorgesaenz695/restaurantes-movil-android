package pe.com.bvl.votacionmovil.presentation.model

import java.io.Serializable

/**
 * Representation for a [UserView] fetched from an external layer data source
 */
data class UserView(var name: String? = null, var lastName: String? = null, var motherLastName: String? = null) :
    Serializable