package pe.com.bvl.votacionmovil.presentation.user

import pe.com.bvl.votacionmovil.presentation.BasePresenter
import pe.com.bvl.votacionmovil.presentation.model.UserView

interface GetUserContract {
    interface View {

        fun showProgress()

        fun hideProgress()

        fun showGetUserSuccess(userView: UserView)

        fun showError(error: Throwable)

        fun hideError()


        fun setPresenter(presenter: Presenter)
    }

    interface Presenter : BasePresenter {
        fun initialize()
    }
}