package pe.com.bvl.votacionmovil.presentation.user

import io.reactivex.observers.DisposableSingleObserver
import pe.com.bvl.votacionmovil.domain.interactor.SingleUseCase
import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.presentation.model.mapper.UserViewMapper
import pe.com.bvl.votacionmovil.domain.interactor.user.GetUserUseCase
import javax.inject.Inject

class GetUserPresenter @Inject constructor(
    val getUserView: GetUserContract.View,
    val getUserUseCase: SingleUseCase<User, Void>,
    val userMapper: UserViewMapper
) :
    GetUserContract.Presenter {


    init {
        getUserView.setPresenter(this)
    }

    override fun start() {
        // initialize()
    }

    override fun onDestroy() {
        getUserView.hideError()
        getUserView.showProgress()
        getUserUseCase.dispose()
    }

    override fun initialize() {
        getUserView.showProgress()

        getUserUseCase.execute(GetUserSubscriber())
    }

    internal fun handleGetUserSuccess(user: User) {
        getUserView.hideProgress()
        getUserView.showGetUserSuccess(userMapper.mapToView(user))

    }

    inner class GetUserSubscriber : DisposableSingleObserver<User>() {

        override fun onSuccess(t: User) {
            handleGetUserSuccess(t)
        }

        override fun onError(exception: Throwable) {
            getUserView.hideProgress()
            getUserView.showError(exception)
        }
    }

}