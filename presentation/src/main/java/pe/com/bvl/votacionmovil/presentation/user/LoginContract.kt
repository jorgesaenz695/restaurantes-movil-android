package pe.com.bvl.votacionmovil.presentation.user

import pe.com.bvl.votacionmovil.presentation.BasePresenter
import pe.com.bvl.votacionmovil.presentation.model.UserView

interface LoginContract {
    interface View {

        fun showProgress()

        fun hideProgress()

        fun showLoginSuccess(userView: UserView)

        fun showError(error: Throwable)

        fun hideError()

        fun setPresenter(presenter: Presenter)
    }

    interface Presenter : BasePresenter {
        fun initialize(email: String, password: String)
    }
}