package pe.com.bvl.votacionmovil.presentation.model.mapper

import pe.com.bvl.votacionmovil.domain.model.User
import pe.com.bvl.votacionmovil.presentation.model.UserView
import javax.inject.Inject

/**
 * Map a [UserView] to and from a [User] instance when data is moving between
 * this layer and the Domain layer
 */
open class UserViewMapper @Inject constructor() : ViewMapper<UserView, User> {


    /**
     * Map a [UserView] instance to a [User] instance
     */
    override fun mapFromView(type: UserView): User {

        return User(type.name, type.lastName, type.motherLastName)
    }

    /**
     * Map a [User] instance to a [UserView] instance
     */
    override fun mapToView(type: User): UserView {
        return UserView(type.name, type.lastName, type.motherLastName)
    }


}