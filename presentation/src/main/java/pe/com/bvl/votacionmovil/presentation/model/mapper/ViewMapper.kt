package pe.com.bvl.votacionmovil.presentation.model.mapper

/**
 * Interface for model mappers. It provides helper methods that facilitate
 * retrieving of models from outer layers
 *
 * @param <V> the view model input type
 * @param <D> the domain model output type
 */
interface ViewMapper<V, D> {

    fun mapToView(type: D): V

    fun mapFromView(type: V): D

}